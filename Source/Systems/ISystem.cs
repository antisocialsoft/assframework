﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Systems
{
    public abstract class ISystem
    {
        public abstract void StandardUpdate(float dt);
        public abstract void FixedUpdate(float step);
    }
}
