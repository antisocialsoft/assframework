﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace AssGameFramework.SceneGraph
{
    public class DrawableComponent : EntityComponent, IDrawableComponent
    {
        public DepthStencilState StencilState { get; set; } = null;
        public BlendState BlendState { get; set; } = null;

        public virtual void Draw(float dt, SpriteBatch spriteBatch)
        {
        }
    }
}
