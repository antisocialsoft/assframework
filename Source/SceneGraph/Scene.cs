﻿using AssGameFramework.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.SceneGraph
{
    public sealed partial class Entity
    {
        //Scene that Entity is member of
        public Scene SceneOwner { get; private set; }

        /**
        * Set Scene
        * 
        * Internal method for handling being added to the scene
        **/
        internal void SetScene(Scene sceneOwner)
        {
            if (SceneOwner == sceneOwner)
                return;

            if (Parent != null && Parent.SceneOwner != sceneOwner)
            {
                Parent.RemoveChildEntity(this);
            }

            SceneOwner = sceneOwner;

            foreach(EntityStatusPair child in _children)
            {
                if (child._toRemove == false)
                {
                    child._ent.SetScene(sceneOwner);
                }
            }

            foreach(ComponentStatusPair comp in _components)
            {
                if (comp._toRemove == false)
                {
                    if (sceneOwner == null)
                    {
                        comp._comp.OnRemovedFromScene();
                    }
                    else
                    {
                        comp._comp.OnAddedToScene();
                    }
                }
            }
        }
    }

    public sealed class Scene
    {
        // List of Entities on Scene
        public List<Entity> Entities { get; private set; }

        private WeakReference<Camera2DComponent> _activeCamera = new WeakReference<Camera2DComponent>(null);

        internal struct EntityAction
        {
            public Entity _ent;
            public bool _add;
        }

        private List<EntityAction> _entitiesToUpdate = new List<EntityAction>();
        private bool _updating = false;

        /**
         * Constructor
         **/
        public Scene()
        {
            Entities = new List<Entity>();
        }

        /**
        * Add Entity
        * 
        * Add a new entity to the scene
        **/
        public void AddEntity(Entity newEnt)
        {
            if (_updating)
            {
                _entitiesToUpdate.Add(new EntityAction{ _ent = newEnt, _add = true });
            }
            else
            {
                newEnt.SetScene(this);
                Entities.Add(newEnt);
            }
        }

        /**
        * Remove Entity
        * 
        * Remove an entity from the scene
        **/
        public void RemoveEntity(Entity entity)
        {
            if (_updating)
            {
                _entitiesToUpdate.Add(new EntityAction { _ent = entity, _add = false });
            }
            else
            {
                Entities.Remove(entity);
                entity.SetScene(null);
            }
        }

        public Entity GetEntity(String entityName)
        {
            Predicate<Entity> search = delegate (Entity compare)
            {
                return (compare.Name == entityName);
            };

            Entity foundEnt = Entities.Find(search);

            return foundEnt;
        }

        public Entity GetEntity(uint entityID)
        {
            Predicate<Entity> search = delegate (Entity compare)
            {
                return (compare.EntityID == entityID);
            };

            Entity foundEnt = Entities.Find(search);

            return foundEnt;
        }

        public T GetComponent<T>() where T : EntityComponent
        {
            foreach (Entity ent in Entities)
            {
                T comp = ent.GetComponent<T>();
                if (comp != null)
                {
                    return comp;
                }
            }

            return null;
        }

        public void GetComponents<T>(ref List<T> components) where T : EntityComponent
        {
            foreach (Entity child in Entities)
            {
                components.AddRange(child.GetComponentsRecursive<T>());
            }
        }

        /**
        * Standard Update
        * 
        * Updates the scene with a non-fixed time step
        **/
        public void StandardUpdate(float dt)
        {
            _updating = true;
            foreach (Entity ent in Entities)
            {
                ent.StandardUpdate(dt);
            }
            _updating = false;
            FinishUpdate();
        }

        /**
        * Fixed Update
        * 
        * Updates the scene with a fixed time step
        **/
        public void FixedUpdate(float step)
        {
            _updating = true;
            foreach (Entity ent in Entities)
            {
                ent.FixedUpdate(step);
            }
            _updating = false;
            FinishUpdate();
        }

        /**
        * Late Update
        * 
        * Updates the scene with a non-fixed time step after systems have updated
        **/
        public void LateUpdate(float dt)
        {
            _updating = true;
            foreach (Entity ent in Entities)
            {
                ent.LateUpdate(dt);
            }
            _updating = false;
            FinishUpdate();
        }
        /**
        * Late Fixed Update
        * 
        * Updates the scene with a fixed time step after systems have updated
        **/
        public void LateFixedUpdate(float step)
        {
            _updating = true;
            foreach (Entity ent in Entities)
            {
                ent.LateFixedUpdate(step);
            }
            _updating = false;
            FinishUpdate();
        }

        /**
        * Draw
        * 
        * Draws the scene
        **/
        public void Draw(float dt, SpriteBatch spriteBatch)
        {
            Camera2DComponent activeCamera = new Camera2DComponent();
            Matrix view = Matrix.Identity;

            if(_activeCamera != null || !_activeCamera.TryGetTarget(out activeCamera))
            {
                activeCamera = GetComponent<Camera2DComponent>();
                _activeCamera.SetTarget(activeCamera);
            }

            if(_activeCamera != null && _activeCamera.TryGetTarget(out activeCamera))
            {
                view = activeCamera.ViewMatrix;
            }

            List<DrawableComponent> drawables = new List<DrawableComponent>();

            GetComponents<DrawableComponent>(ref drawables);
            
            Comparison<DrawableComponent> drawableCompare = delegate (DrawableComponent lhs, DrawableComponent rhs)
            {
                return lhs.Entity.WorldPosition3D.Z.CompareTo(rhs.Entity.WorldPosition3D.Z);
            };

            drawables.Sort(drawableCompare);

            BlendState currentBlendState = BlendState.AlphaBlend;

            spriteBatch.Begin(SpriteSortMode.Immediate, rasterizerState: RasterizerState.CullNone, transformMatrix:view, blendState: currentBlendState);

            foreach (DrawableComponent drawable in drawables)
            {
                if (drawable.BlendState == null)
                {
                    if (currentBlendState != BlendState.AlphaBlend)
                    {
                        currentBlendState = BlendState.AlphaBlend;
                        spriteBatch.End();
                        spriteBatch.Begin(transformMatrix: view, rasterizerState: RasterizerState.CullNone, blendState: currentBlendState);
                    }
                }
                else if(drawable.BlendState != currentBlendState)
                {
                    currentBlendState = drawable.BlendState;
                    spriteBatch.End();
                    spriteBatch.Begin(SpriteSortMode.Immediate, rasterizerState: RasterizerState.CullNone, transformMatrix: view, blendState: currentBlendState);
                }

                drawable.Draw(dt, spriteBatch);
            }

            spriteBatch.End();
        }

        private void FinishUpdate()
        {
            foreach(EntityAction entityAction in _entitiesToUpdate)
            {
                if(entityAction._add)
                {
                    AddEntity(entityAction._ent);
                }
                else
                {
                    RemoveEntity(entityAction._ent);
                }
            }

            _entitiesToUpdate.Clear();
        }
    }
}
