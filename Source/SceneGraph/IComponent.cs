﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.SceneGraph
{
    public interface IComponent
    {
        Entity Entity { get; }
        bool Enabled { get; set; }
    }
}
