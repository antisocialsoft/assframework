﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.SceneGraph
{
    public sealed partial class Entity
    {
        private bool _enabled = true;
        public bool Enabled
        {
            get
            {
                return _enabled && (SceneOwner != null) && ((Parent != null && Parent.Enabled) || Parent == null);
            }
            set
            {

                if (value)
                {
                    Enable();
                } else
                {
                    Disable();
                }

                _enabled = value;
            }
        }

        /**
         * Called if the entity is enabled
         *
         * This method is private - use the OnEnabled methode in your component
         */
        private void Enable()
        {
            foreach (Entity sub in Children)
            {
                sub.Enable();
            }

            foreach (EntityComponent comp in Components)
            {
                comp.Enable();
            }
        }

        /**
         * Called if the entity is disabled
         *
         * This method is private - use the OnDisabled methode in your component
         */
        private void Disable()
        {
            foreach (Entity sub in Children)
            {
                sub.Enable();
            }

            foreach (EntityComponent comp in Components)
            {
                comp.Disable();
            }
        }

        /**
         * Add Component
         * 
         * Adds a new component to the Component List
         **/
        public void AddComponent(EntityComponent entComp)
        {
            _components.Add(new ComponentStatusPair(entComp));

            entComp.AddToEntity(this);
        }

         /**
         * Remove Component
         * 
         * Remove an existing component from the Component List
         **/ 
        public void RemoveComponent(EntityComponent entComp)
        {
            Predicate<ComponentStatusPair> search = delegate (ComponentStatusPair compare)
            {
                return (compare._comp == entComp);
            };

            ComponentStatusPair status = _components.Find(search);

            if (status != null && status._toRemove == false)
            {
                status._toRemove = true;

                entComp.RemoveFromEntity();
            }
        }
    }

    public class EntityComponent : IComponent
    {
        //Owning Entity (can be null)
        public Entity Entity { get; private set; }

        private bool _enabled = true;

        public bool Enabled
        {
            get
            {
                return Entity != null && Entity.Enabled && _enabled;
            }
            set
            {
                _enabled = value;
            }
        }

        public bool Destroyed { get; private set; }

        public void Destroy()
        {
            Destroyed = true;
            RemoveFromEntity();
            OnDestroy();
        }

        protected virtual void OnDestroy()
        {
        }

        /**
         * Add To Entity
         * 
         * Internal function for handling this component being added to an Entity
         **/
        internal void AddToEntity(Entity newParent)
        {
            bool validParent = (Entity != null) && (Entity != newParent);

            if (validParent)
            {
                RemoveFromEntity();
            }

            validParent = (Entity == null) || Entity != newParent;

            Entity = newParent;

            if (validParent)
            {
                OnAddToEntity(newParent);
            }
        }

        /**
         * Remove From Entity
         * 
         * Internal function for handling this component being removed from an Entity
         **/ 
        internal void RemoveFromEntity()
        {
            if (Entity != null)
            {
                OnRemoveFromEntity();
                Entity = null;
            }
        }

        /**
        * Add To Entity
        * 
        * Internal function for handling this component being added to an Entity
        **/
        internal void Enable()
        {
            Debug.Assert(Entity != null, "Can't enable the component because it wasn't added to an entity in the first place");
            OnEnabled();
        }

        /**
         * Remove From Entity
         * 
         * Internal function for handling this component being removed from an Entity
         **/
        internal void Disable()
        {
            Debug.Assert(Entity != null, "Can't disable the component because it wasn't added to an entity in the first place");
            OnDisabled();
        }

        /**
        * On Entity Enabled
        * 
        * Override method for handling logic when the entity was enabled
        **/
        protected virtual void OnEnabled()
        {

        }

        /**
        * On Entity Disable
        * 
        * Override method for handling logic when the entity was disabled
        **/
        protected virtual void OnDisabled()
        {

        }

        /**
        * On Add To Entity
        * 
        * Override method for handling logic when component is added to an entity
        **/
        protected virtual void OnAddToEntity(Entity newParent)
        {

        }

        /**
        * On Remove From Entity
        * 
        * Override method for handling logic when component is removed from an entity
        **/
        protected virtual void OnRemoveFromEntity()
        {

        }

        public virtual void OnRemovedFromScene()
        {
            
        }

        public virtual void OnAddedToScene()
        {
            
        }
    }
}
