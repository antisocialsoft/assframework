﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.SceneGraph
{
    public class UpdatableComponent : EntityComponent, IUpdatableComponent
    {
        public virtual void FixedUpdate(float step)
        {
        }

        public virtual void LateFixedUpdate(float step)
        {
        }

        public virtual void LateUpdate(float dt)
        {
        }

        public virtual void StandardUpdate(float dt)
        {
        }
    }
}
