﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.SceneGraph
{
    public interface IUpdatableComponent
    {
        void StandardUpdate(float dt);
        void FixedUpdate(float step);
        void LateUpdate(float dt);
        void LateFixedUpdate(float step);
    }
}
