﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.SceneGraph
{
    internal interface IDrawableComponent
    {
        void Draw(float dt, SpriteBatch spriteBatch);
    }
}
