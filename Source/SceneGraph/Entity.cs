﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.SceneGraph
{
    public sealed partial class Entity
    {
        private class EntityStatusPair
        {
            public EntityStatusPair(Entity ent) { _ent = ent;  }

            public Entity _ent;
            public bool _toRemove;
        };

        private class ComponentStatusPair
        {
            public ComponentStatusPair(EntityComponent comp) { _comp = comp; }

            public EntityComponent _comp;
            public bool _toRemove;
        };

        static private uint entityCount = 0;
        public uint EntityID { get; private set; }

        public String Name { get; set; }

        public Entity Parent { get; private set; }
        public List<Entity> Children
        {
            get
            {
                List<Entity> children = new List<Entity>();
                foreach (EntityStatusPair childPair in _children)
                {
                    if (!childPair._toRemove)
                    {
                        children.Add(childPair._ent);
                    }
                }
                return children;
            }
        }

        public List<EntityComponent> Components
        {
            get
            {
                List<EntityComponent> components = new List<EntityComponent>();
                foreach (ComponentStatusPair compPair in _components)
                {
                    if (compPair._toRemove)
                    {
                        components.Add(compPair._comp);
                    }
                }
                return components;
            }
        }

        public bool Destroyed { get; private set; }

        public delegate void TransformChangedDelegate();

        public Vector2 LocalPosition
        {
            get
            {
                return new Vector2(_localPosition.X, _localPosition.Y);
            }
            set
            {
                LocalPosition3D = new Vector3(value.X, value.Y, _localPosition.Z);
            }
        }

        public float LocalRotation
        {
            get
            {
                Quaternion q = _localRotation;
                double siny = +2.0 * (q.W * q.Z + q.X * q.Y);
                double cosy = +1.0 - 2.0 * (q.Y * q.Y + q.Z * q.Z);
                return (float)Math.Atan2(siny, cosy);
            }
            set
            {
                LocalOrientation = Quaternion.CreateFromAxisAngle(Vector3.UnitZ, value);
                OnTransformChanged();
            }
        }

        public Vector2 LocalScale
        {
            get
            {
                return new Vector2(_localScale.X, _localScale.Y);
            }
            set
            {
                LocalScale3D = new Vector3(value.X, value.Y, _localScale.Z);
            }
        }

        public Vector3 LocalPosition3D
        {
            get
            {
                return _localPosition;
            }
            set
            {
                Debug.Assert(!float.IsNaN(value.X));
                Debug.Assert(!float.IsNaN(value.Y));
                Debug.Assert(!float.IsNaN(value.Z));
                _localPosition = value;
                _dirtyTransform = true;
                OnTransformChanged();

            }
        }
        public Quaternion LocalOrientation
        {
            get
            {
                return _localRotation;
            }
            set
            {
                _localRotation = value;
                _dirtyTransform = true;
                OnTransformChanged();

            }
        }

        public Vector3 LocalScale3D
        {
            get
            {
                return _localScale;
            }
            set
            {
                _localScale = value;
                _dirtyTransform = true;
                OnTransformChanged();

            }
        }

        public Matrix LocalTransform {
            get
            {
                if(_dirtyTransform || true)
                {
                    Quaternion q = _localRotation;

                    float wSquared = q.W * q.W;
                    float xSquared = q.X * q.X;
                    float ySquared = q.Y * q.Y;
                    float zSquared = q.Z * q.Z;

                    float a = wSquared + xSquared - ySquared - zSquared;
                    float b = 2 * q.X * q.Y + 2 * q.W * q.Z;
                    float c = 2 * q.X * q.Z - 2 * q.W * q.Y;
                    float d = 0;
                    float e = 2 * q.X * q.Y - 2 * q.W * q.Z;
                    float f = wSquared - xSquared + ySquared - zSquared;
                    float g = 2 * q.Y * q.Z + 2 * q.W * q.X;
                    float h = 0;
                    float i = 2 * q.X * q.Z + 2 * q.W * q.Y;
                    float j = 2 * q.Y * q.Z - 2 * q.W * q.X;
                    float k = wSquared - xSquared - ySquared + zSquared;
                    float l = 0;
                    float m = 0;
                    float n = 0;
                    float o = 0;
                    float p = 1;

                    //Matrix < float, 4 > (
                    //1.0f - 2.0f * qy * qy - 2.0f * qz * qz,
                    //2.0f * qx * qy - 2.0f * qz * qw,
                    //2.0f * qx * qz + 2.0f * qy * qw,
                    //0.0f,
                    //2.0f * qx * qy + 2.0f * qz * qw,
                    //1.0f - 2.0f * qx * qx - 2.0f * qz * qz,
                    //2.0f * qy * qz - 2.0f * qx * qw,
                    //0.0f,
                    //2.0f * qx * qz - 2.0f * qy * qw,
                    //2.0f * qy * qz + 2.0f * qx * qw,
                    //1.0f - 2.0f * qx * qx - 2.0f * qy * qy,
                    //0.0f,
                    //0.0f,
                    //0.0f,
                    //0.0f,
                    //1.0f);

                    //float a = 1.0f - 2.0f * ySquared - 2.0f * zSquared;
                    //float b = 2.0f * q.X * q.Y + 2.0f * q.W * q.Z;
                    //float c = 2.0f * q.X * q.Z - 2.0f * q.W * q.Y;
                    //float d = 0;
                    //float e = 2.0f * q.X * q.Y - 2.0f * q.W * q.Z;
                    //float f = 1.0f - 2.0f * xSquared - 2.0f * zSquared;
                    //float g = 2.0f * q.Y * q.Z - 2.0f * q.W * q.X;
                    //float h = 0;
                    //float i = 2.0f * q.X * q.Z - 2.0f * q.W * q.Y;
                    //float j = 2.0f * q.Y * q.Z + 2.0f * q.W * q.X;
                    //float k = 1.0f - 2.0f * xSquared - 2.0f * ySquared;
                    //float l = 0;
                    //float m = 0;
                    //float n = 0;
                    //float o = 0;
                    //float p = 1;

                    Matrix rotation = new Matrix(
                        a, b, c, d,
                        e, f, g, h,
                        i, j, k, l,
                        m, n, o, p);

                    //_localTransform = new Matrix(
                    //    _localScale.X * rotation[0], _localScale.X * rotation[1], _localScale.X * rotation[2], 0,
                    //    _localScale.Y * rotation[4], _localScale.Y * rotation[5], _localScale.Y * rotation[6], 0,
                    //    _localScale.Z * rotation[8], _localScale.Z * rotation[9], _localScale.Z * rotation[10], 0,
                    //    _localPosition.X, _localPosition.Y, _localPosition.Z, 1);
                    if (Parent != null)
                    {
                        Vector3 parentScale = Parent.WorldScale3D;
                        _localTransform = new Matrix(
                        parentScale.X * rotation[0], parentScale.X * rotation[1], parentScale.X * rotation[2], 0,
                        parentScale.Y * rotation[4], parentScale.Y * rotation[5], parentScale.Y * rotation[6], 0,
                        parentScale.Z * rotation[8], parentScale.Z * rotation[9], parentScale.Z * rotation[10], 0,
                        _localPosition.X, _localPosition.Y, _localPosition.Z, 1);
                    }
                    else
                    {
                        _localTransform = new Matrix(
                           rotation[0], rotation[1], rotation[2], 0,
                            rotation[4], rotation[5], rotation[6], 0,
                            rotation[8], rotation[9], rotation[10], 0,
                            _localPosition.X, _localPosition.Y, _localPosition.Z, 1);
                    }

                    _dirtyTransform = false;
                }

                return _localTransform;

            }
                set
            {
                _localTransform = value;
                _localTransform.Decompose(out _localScale, out _localRotation, out _localPosition);
                _dirtyTransform = false;
            }
        } 

        public Matrix WorldTransform
        {
            get {
                if(Parent != null)
                {
                    Matrix transform = LocalTransform * Parent.WorldTransform;
                    return transform;

                }
                else
                {
                    return LocalTransform;
                }
            }
        }

        public Vector2 WorldPosition { get {
                Vector3 worldTrans = WorldPosition3D;
                return new Vector2(worldTrans.X, worldTrans.Y) ; } }
        public float WorldRotation { get
            {
                Quaternion q = WorldOrientation;
                double siny = +2.0 * (q.W * q.Z + q.X * q.Y);
                double cosy = +1.0 - 2.0 * (q.Y * q.Y + q.Z * q.Z);
                return (float)Math.Atan2(siny, cosy);


                //q.X = 0;
                //q.Z = 0;
                //float mag = (float)Math.Sqrt(q.W * q.W + q.Y * q.Y);
                //q.W /= mag;
                //q.Y /= mag;

                //return 2.0f * (float)Math.Acos(q.W);

                //return (float)Math.Atan2(2.0 * (q.Y * q.Z + q.W * q.X), q.W * q.W - q.X * q.X - q.Y * q.Y + q.Z * q.Z);
            }
        }
        public Vector2 WorldScale { get { if (Parent != null) return Parent.WorldScale * LocalScale; else return LocalScale;  } }

        public Vector3 WorldPosition3D { get { return WorldTransform.Translation; } }
        public Quaternion WorldOrientation { get { return Quaternion.CreateFromRotationMatrix(WorldTransform); } }
        public Vector3 WorldScale3D { get { if (Parent != null) return Parent.WorldScale3D * LocalScale3D; else return LocalScale3D; } }

        Vector3 _localPosition;
        Quaternion _localRotation;
        Vector3 _localScale;

        private event TransformChangedDelegate _transformChangedEvent;

        private List<EntityStatusPair> _children;
        private List<ComponentStatusPair> _components;

        Matrix _localTransform = Matrix.Identity;
        bool _dirtyTransform = false;

        /**
         * Constructor
         **/
        public Entity()
        {
            _children = new List<EntityStatusPair>();
            _components = new List<ComponentStatusPair>();

            LocalPosition3D = Vector3.Zero;
            LocalRotation = 0;
            LocalScale3D = Vector3.One;
            Destroyed = false;

            EntityID = entityCount++;
            Name = String.Format("Entity_{0:D3}", EntityID);
    }

        ~Entity()
        {
            Console.WriteLine("Deleting Entity.");
        }

        public void SubscribeToTransformChangedEvent(TransformChangedDelegate changeDelegate)
        {
            _transformChangedEvent += changeDelegate;
        }

        public void UnsubscribeToTransformChangedEvent(TransformChangedDelegate changeDelegate)
        {
            _transformChangedEvent -= changeDelegate;
        }

        /**
         * Add Child Entity
         * 
         * Add a new child to the entity
         * 
         **/
        public void AddChildEntity(Entity newChild)
        {
            foreach (EntityStatusPair entStatus in _children)
            {
                if (entStatus._ent == newChild)
                {
                    entStatus._toRemove = false;
                    return;
                }
            }
            _dirtyTransform = true;
            _children.Add(new EntityStatusPair(newChild));

            newChild.AddToParent(this);
            newChild.SetScene(SceneOwner);
        }

        /**
         * Remove Child Entity
         * 
         * Removes the specified child from the entity
         * 
         **/
        public void RemoveChildEntity(Entity child)
        {
            Predicate<EntityStatusPair> search = delegate (EntityStatusPair compare)
            {
                return (compare._ent == child && compare._toRemove == false);
            };

            EntityStatusPair status = _children.Find(search);

            if(status != null && status._toRemove == false)
            {
                status._toRemove = true;

                child.RemoveFromParent();
                child.SetScene(null);
            }
        }

        public Entity GetChildEntity(String entityName, bool recursive)
        {
            Predicate<EntityStatusPair> search = delegate (EntityStatusPair compare)
            {
                return (compare._ent.Name == entityName && compare._toRemove == false);
            };

            EntityStatusPair status = _children.Find(search);

            if(status != null)
            {
                return status._ent;
            }
            else if(recursive)
            {
                foreach(EntityStatusPair childStatus in _children)
                {
                    if (childStatus._toRemove == false)
                    {
                        Entity foundEnt = childStatus._ent.GetChildEntity(entityName, recursive);
                        if(foundEnt != null)
                        {
                            return foundEnt;
                        }
                    }
                }
            }

            return null;
        }

        public Entity GetChildEntity(uint entityID, bool recursive)
        {
            Predicate<EntityStatusPair> search = delegate (EntityStatusPair compare)
            {
                return (compare._ent.EntityID == entityID && compare._toRemove == false);
            };

            EntityStatusPair status = _children.Find(search);

            if (status != null)
            {
                return status._ent;
            }
            else if (recursive)
            {
                foreach (EntityStatusPair childStatus in _children)
                {
                    if (childStatus._toRemove == false)
                    {
                        Entity foundEnt = childStatus._ent.GetChildEntity(entityID, recursive);
                        if (foundEnt != null)
                        {
                            return foundEnt;
                        }
                    }
                }
            }

            return null;
        }

        /**
         * GetComponent T
         * 
         * Finds the first T in the component list and returns it
         * 
         **/
        public T GetComponent<T>() where T : EntityComponent
        {
            foreach (ComponentStatusPair comp in _components)
            {
                if (comp._comp is T && comp._toRemove == false)
                {
                    return (T)comp._comp;
                }
            }

            return null;
        }

        /**
         * Get Component Recursive T
         * 
         * Finds the first T in the component list and returns it
         * If no T is found, recurse through the children.
         * 
         **/
        public T GetComponentRecursive<T>() where T : EntityComponent
        {
            foreach (ComponentStatusPair comp in _components)
            {
                if (comp._comp is T && comp._toRemove == false)
                {
                    return (T)comp._comp;
                }
            }

            foreach (EntityStatusPair child in _children)
            {
                if (child._toRemove == false)
                {
                    EntityComponent comp = child._ent.GetComponentRecursive<T>();
                    if (comp != null)
                    {
                        return (T)comp;
                    }
                }
            }

            return null;
        }

        /**
        * Get Components T
        * 
        * Gets all T components and returns them as a list
        * 
        **/
        public List<T> GetComponents<T>() where T : EntityComponent
        {
            Predicate<ComponentStatusPair> search = delegate (ComponentStatusPair compare)
            {
                return (compare._comp is T && compare._toRemove == false);
            };

            List<ComponentStatusPair> pairs = _components.FindAll(search);
            List<T> list = new List<T>();

            foreach(ComponentStatusPair pair in pairs)
            {
                list.Add((T)(pair._comp));
            }

            return list;
        }

        /**
        * Get Components Recursive T
        * 
        * Gets all T components in component list and children's components lists
        * 
        **/
        public List<T> GetComponentsRecursive<T>() where T : EntityComponent
        {
            List<T> comps = GetComponents<T>();
            foreach (EntityStatusPair child in _children)
            {
                if (child._toRemove == false)
                {
                    comps.AddRange(child._ent.GetComponentsRecursive<T>());
                }
            }

            return comps;
        }

        internal void Destroy()
        {
            if (Parent != null)
            {
                Parent.RemoveChildEntity(this);
            }

            int childCount = _children.Count();
            for (int i = 0; i < childCount; ++i)
            {
                EntityStatusPair entPair = _children[i];
                RemoveChildEntity(entPair._ent);
                entPair._ent.Destroy();
            }

            int compCount = _components.Count();
            for (int i = 0; i < compCount; ++i)
            {
                ComponentStatusPair compPair = _components[i];
                RemoveComponent(compPair._comp);
                compPair._comp.Destroy();
            }
            if (SceneOwner != null)
            {
                SceneOwner.RemoveEntity(this);
            }

            Destroyed = true;
        }

        /**
        * Standard Update
        * 
        * Updates the entity components then children with delta time
        * 
        **/
        public void StandardUpdate(float dt)
        {
            if(Destroyed || !Enabled)
            {
                return;
            }

            bool updateRequired = false;

            for (int i = 0; i < _components.Count; ++i)
            {
                ComponentStatusPair comp = _components[i];

                if (comp._comp is IUpdatableComponent && comp._toRemove == false)
                {
                    ((IUpdatableComponent)comp._comp).StandardUpdate(dt);

                    if(comp._toRemove == true)
                    {
                        updateRequired = true;
                    }
                }
            }

            if(updateRequired)
            {
                for(int i = 0; i < _components.Count; ++i)
                {
                    if(_components[i]._toRemove)
                    {
                        _components.RemoveAt(i);
                        --i;
                    }
                }

                updateRequired = false;
            }

            for (int i = 0; i < _children.Count; ++i)
            {
                if (_children[i]._toRemove == false)
                {
                    Entity child = _children[i]._ent;
                    child.StandardUpdate(dt);
                }
            }

            if (updateRequired)
            {
                for (int i = 0; i < _children.Count; ++i)
                {
                    if (_children[i]._toRemove)
                    {
                        _children.RemoveAt(i);
                        --i;
                    }
                }
            }
        }

        /**
        * Fixed Update
        * 
        * Updates the entity components then children with a fixed time step
        * 
        **/
        public void FixedUpdate(float step)
        {
            if (Destroyed || !Enabled)
            {
                return;
            }

            bool updateRequired = false;

            for (int i = 0; i < _components.Count; ++i)
            {
                ComponentStatusPair comp = _components[i];

                if (comp._comp is IUpdatableComponent && comp._toRemove == false)
                {
                    ((IUpdatableComponent)comp._comp).FixedUpdate(step);
                }

                updateRequired |= comp._toRemove;
            }

            if (updateRequired)
            {
                for (int i = 0; i < _components.Count; ++i)
                {
                    if (_components[i]._toRemove)
                    {
                        _components.RemoveAt(i);
                        --i;
                    }
                }

                updateRequired = false;
            }

            for (int i = 0; i < _children.Count; ++i)
            {
                if (_children[i]._toRemove == false)
                {
                    Entity child = _children[i]._ent;
                    child.FixedUpdate(step);
                }
                else
                {
                    updateRequired = true;
                }
            }

            if (updateRequired)
            {
                for (int i = 0; i < _children.Count; ++i)
                {
                    if (_children[i]._toRemove)
                    {
                        _children.RemoveAt(i);
                        --i;
                    }
                }
            }
        }

        /**
        * Late Update
        * 
        * Non-fixed update after systems have updated
        * 
        **/
        public void LateUpdate(float dt)
        {
            if (Destroyed || !Enabled)
            {
                return;
            }

            bool updateRequired = false;

            for (int i = 0; i < _components.Count; ++i)
            {
                ComponentStatusPair comp = _components[i];

                if (comp._comp is IUpdatableComponent && comp._toRemove == false)
                {
                    ((IUpdatableComponent)comp._comp).LateUpdate(dt);
                }
            }

            if (updateRequired)
            {
                for (int i = 0; i < _components.Count; ++i)
                {
                    if (_components[i]._toRemove)
                    {
                        _components.RemoveAt(i);
                        --i;
                    }
                }

                updateRequired = false;
            }

            for (int i = 0; i < _children.Count; ++i)
            {
                if (_children[i]._toRemove == false)
                {
                    Entity child = _children[i]._ent;
                    child.LateUpdate(dt);
                }
            }

            if (updateRequired)
            {
                for (int i = 0; i < _children.Count; ++i)
                {
                    if (_children[i]._toRemove)
                    {
                        _children.RemoveAt(i);
                        --i;
                    }
                }
            }
        }

        /**
        * Late Fixed Update
        * 
        * Fixed update after systems have updated
        * 
        **/
        public void LateFixedUpdate(float step)
        {
            if (Destroyed || !Enabled)
            {
                return;
            }

            bool updateRequired = false;

            for (int i = 0; i < _components.Count; ++i)
            {
                ComponentStatusPair comp = _components[i];

                if (comp._comp is IUpdatableComponent && comp._toRemove == false)
                {
                    ((IUpdatableComponent)comp._comp).LateFixedUpdate(step);
                }
            }

            if (updateRequired)
            {
                for (int i = 0; i < _components.Count; ++i)
                {
                    if (_components[i]._toRemove)
                    {
                        _components.RemoveAt(i);
                        --i;
                    }
                }

                updateRequired = false;
            }

            for (int i = 0; i < _children.Count; ++i)
            {
                if (_children[i]._toRemove == false)
                {
                    Entity child = _children[i]._ent;
                    child.LateFixedUpdate(step);
                }
            }

            if (updateRequired)
            {
                for (int i = 0; i < _children.Count; ++i)
                {
                    if (_children[i]._toRemove)
                    {
                        _children.RemoveAt(i);
                        --i;
                    }
                }
            }
        }

        /**
        * Draw
        * 
        * Draw any IDrawableComponents
        * 
        **/
        public void Draw(float dt, SpriteBatch spriteBatch)
        {
            if (Destroyed || !Enabled)
            {
                return;
            }

            for (int i = 0; i < _components.Count; ++i)
            {
                ComponentStatusPair comp = _components[i];

                if (comp._comp is IDrawableComponent && comp._toRemove == false)
                {
                    ((IDrawableComponent)comp._comp).Draw(dt, spriteBatch);
                }
            }

            for (int i = 0; i < _children.Count; ++i)
            {
                if (_children[i]._toRemove == false)
                {
                    Entity child = _children[i]._ent;
                    child.Draw(dt, spriteBatch);
                }
            }
        }

        private void OnTransformChanged()
        {
            if (_transformChangedEvent != null)
            {
                _transformChangedEvent.Invoke();
            }
        }

        /**
        * Add To Parent
        * 
        * Private function to update parent
        * 
        **/
        private void AddToParent(Entity newParent)
        {
            Debug.Assert(newParent != null, "Cannot add to null. Did you mean to call RemoveFromParent?");

            if (Parent != null && Parent != newParent)
            {
                Parent.RemoveChildEntity(this);
            }

            Parent = newParent;
        }

        private void RemoveFromParent()
        {
            if (Parent != null)
            {
                Parent.RemoveChildEntity(this);
                Parent = null;
            }
        }
    }
}
