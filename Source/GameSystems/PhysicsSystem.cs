﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using AssGameFramework.GameObjects;
using AssGameFramework.SceneGraph;
using AssGameFramework.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameSystems
{
    public class PhysicsSystem : ISystem
    {
        public static float SPRITE_TO_WORLD_SCALE = 0.01f;
        public static float WORLD_TO_SPRITE_SCALE = 1.0f / SPRITE_TO_WORLD_SCALE;

        public World PhysicsWorld { get; private set; }
        public bool Active { get; set; } = true;

        private BroadphaseDelegate _originalBpDelegate;

        public PhysicsSystem(Vector2 gravity)
        {
            PhysicsWorld = new World(gravity);
            PhysicsWorld.ContactManager.BeginContact = BeginContact;
            PhysicsWorld.ContactManager.EndContact = EndContact;
            _originalBpDelegate = PhysicsWorld.ContactManager.OnBroadphaseCollision;
            PhysicsWorld.ContactManager.OnBroadphaseCollision = BroadphaseCollision;
        }

        public bool BeginContact(Contact contact)
        {
            Entity entA = (Entity)contact.FixtureA.UserData;
            Entity entB = (Entity)contact.FixtureB.UserData;

            List<CollisionComponent> colsA = entA?.GetComponents<CollisionComponent>();
            List<CollisionComponent> colsB = entB?.GetComponents<CollisionComponent>();

            bool collision = true;

            if (colsA != null)
            {
                foreach (CollisionComponent col in colsA)
                {
                    collision = col.OnCollisionStart(entB) && collision;
                }
            }

            if (colsB != null)
            {
                foreach (CollisionComponent col in colsB)
                {
                    collision = col.OnCollisionStart(entA) && collision;
                }
            }

            return collision;
        }

        public void EndContact(Contact contact)
        {
            Entity entA = (Entity)contact.FixtureA.UserData;
            Entity entB = (Entity)contact.FixtureB.UserData;

            CollisionComponent colA = entA?.GetComponent<CollisionComponent>();
            CollisionComponent colB = entB?.GetComponent<CollisionComponent>();

            if (colA != null)
            {
                colA.OnCollisionEnded(entB);
            }

            if (colB != null)
            {
                colB.OnCollisionEnded(entA);
            }
        }

        public void BroadphaseCollision(ref FixtureProxy proxyA, ref FixtureProxy proxyB)
        {
            _originalBpDelegate(ref proxyA, ref proxyB);

            Entity entA = (Entity)proxyA.Fixture.UserData;
            Entity entB = (Entity)proxyB.Fixture.UserData;

            CollisionComponent colA = entA?.GetComponent<CollisionComponent>();
            CollisionComponent colB = entB?.GetComponent<CollisionComponent>();

            if (colA != null)
            {
                colA.OnNearMiss(entB);
            }

            if (colB != null)
            {
                colB.OnNearMiss(entA);
            }
        }

        public override void FixedUpdate(float step)
        {
            if(Active)
                PhysicsWorld.Step(step);
        }


        public override void StandardUpdate(float dt)
        {
        }
    }
}
