﻿using AssGameFramework.Core;
using AssGameFramework.Systems;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameSystems
{
    public class AudioSystem : ISystem
    {
        private Dictionary<uint, SoundEffect> _idToSoundMap = new Dictionary<uint, SoundEffect>();
        private Dictionary<string, uint> _fileToIDMap = new Dictionary<string, uint>();

        private Dictionary<uint, List<SoundEffectInstance>> _idToPoolMap = new Dictionary<uint, List<SoundEffectInstance>>();

        private Dictionary<uint, Song> _idToSongMap = new Dictionary<uint, Song>();
        private Dictionary<string, uint> _fileToSongMap = new Dictionary<string, uint>();

        private uint _currentAudioID = 0U;
        

        public float MusicVolume
        {
            get
            {
                return MediaPlayer.Volume;
            }
            set
            {
                MediaPlayer.Volume = value;
            }
        }

        public AudioSystem()
        {
            MusicVolume = 1.0f;
        }

        ~AudioSystem()
        {
        }

        /**
         * Loads the given file into the sound buffer
         * returns the id of the sound in the buffer
         *    -> the id is needed to play the sound
         */
        public uint LoadSoundEffect(string name)
        {
            if(_fileToIDMap.ContainsKey(name))
            {
                if (_idToSoundMap[_fileToIDMap[name]] != null)
                {
                    return _fileToIDMap[name];
                }
                else
                {
                    SoundEffect oldEffect = ApplicationContext.Instance.Content.Load<SoundEffect>(name);
                    _idToSoundMap.Add(_fileToIDMap[name], oldEffect);
                }
                return _fileToIDMap[name];
            }

            SoundEffect soundEffect = ApplicationContext.Instance.Content.Load<SoundEffect>(name);
            _idToSoundMap.Add(_currentAudioID, soundEffect);
            _fileToIDMap.Add(name, _currentAudioID);
            _idToPoolMap.Add(_currentAudioID, new List<SoundEffectInstance>());
            return _currentAudioID++;
        }

        private bool LoadSoundEffect(uint id)
        {
            foreach(string key in _fileToIDMap.Keys)
            {
                if(_fileToIDMap[key] == id)
                {
                    LoadSoundEffect(key);
                    return true;
                }
            }
            return false;
        }

        /**
         * This unloads the sound with the given id from the buffer
         * returns either if it was unloaded or not
         */
        public bool UnloadSoundEffect(uint id)
        {
            if(_idToSoundMap.ContainsKey(id))
            {
                SoundEffect seInstance = _idToSoundMap[id];
                seInstance.Dispose();

                _idToSoundMap.Remove(id);
            }
            return false;
        }

        /**
         * This plays the sound
         * needs parameter id -> the ID of the sound effect you get from the AudioSystem.Load(name) method
         * returns either if the sound started playing or not
         */
        public int PlaySoundEffect(uint id, bool loop = false, bool loadIfPossible = true, float volume = 1.0f, float pitch = 1.0f)
        {
            if (_idToSoundMap.ContainsKey(id))
            {
                SoundEffect soundEffect = _idToSoundMap[id];
                if (soundEffect == null && (loadIfPossible && !LoadSoundEffect(id)))
                {
                    return -1;
                }

                SoundEffectInstance toPlay = null;
                int foundIndex = -1;

                for(int i = 0; i < _idToPoolMap[id].Count; ++i)
                {
                    SoundEffectInstance instance = _idToPoolMap[id][i];
                    if (instance.State == SoundState.Stopped)
                    {
                        toPlay = instance;
                        foundIndex = i;
                        break;
                    }
                }

                if(toPlay == null)
                {
                    toPlay = soundEffect.CreateInstance();
                    _idToPoolMap[id].Add(toPlay);
                    foundIndex = _idToPoolMap[id].Count - 1;
                }

                toPlay.Volume = volume;
                toPlay.Pitch = pitch;
                toPlay.IsLooped = loop;
                toPlay.Play();
                return foundIndex;
            }
            return -1;
        }

        public void StopSoundEffect(uint id, int instanceID, bool pause = false)
        {
            if (_idToSoundMap.ContainsKey(id))
            {
                SoundEffect soundEffect = _idToSoundMap[id];
                if (soundEffect == null)
                {
                    return;
                }

                if(_idToPoolMap[id].Count > instanceID)
                {
                    if(pause)
                    {
                        _idToPoolMap[id][instanceID].Pause();
                    }
                    else
                    {
                        _idToPoolMap[id][instanceID].Stop();
                    }
                }

            }
        }

        public void UnpauseSoundEffect(uint id, int instanceID)
        {
            if (_idToSoundMap.ContainsKey(id))
            {
                SoundEffect soundEffect = _idToSoundMap[id];
                if (soundEffect == null)
                {
                    return;
                }

                if (_idToPoolMap[id].Count > instanceID)
                {
                        _idToPoolMap[id][instanceID].Resume();
                }
            }
        }

        public void ChangeSoundEffectVolume(uint id, int instanceID, float volume)
        {
            if (_idToSoundMap.ContainsKey(id))
            {
                SoundEffect soundEffect = _idToSoundMap[id];
                if (soundEffect == null)
                {
                    return;
                }

                if (_idToPoolMap[id].Count > instanceID)
                {
                    _idToPoolMap[id][instanceID].Volume = volume;
                }
            }
        }

        public uint LoadSong(string name)
        {
            if (_fileToSongMap.ContainsKey(name))
            {
                if (_idToSongMap[_fileToSongMap[name]] != null)
                {
                    return _fileToSongMap[name];
                }
                else
                {
                    Song oldSong = ApplicationContext.Instance.Content.Load<Song>(name);
                    _idToSongMap.Add(_fileToIDMap[name], oldSong);
                }
                return _fileToSongMap[name];
            }

            Song newSong = ApplicationContext.Instance.Content.Load<Song>(name);
            _idToSongMap.Add(_currentAudioID, newSong);
            _fileToSongMap.Add(name, _currentAudioID);
            return _currentAudioID++;
        }

        private bool LoadSong(uint id)
        {
            foreach (string key in _fileToSongMap.Keys)
            {
                if (_fileToSongMap[key] == id)
                {
                    LoadSong(key);
                    return true;
                }
            }
            return false;
        }

        /**
         * This unloads the sound with the given id from the buffer
         * returns either if it was unloaded or not
         */
        public bool UnloadSong(uint id)
        {
            if (_idToSongMap.ContainsKey(id))
            {
                Song song = _idToSongMap[id];
                song.Dispose();

                _idToSongMap.Remove(id);
            }
            return false;
        }

        public bool PlaySong(uint songID, bool repeat = true, bool loadIfPossible = true)
        {
            if (_idToSongMap.ContainsKey(songID))
            {
                Song song = _idToSongMap[songID];
                if (song == null && (loadIfPossible && !LoadSong(songID)))
                {
                    return false;
                }

                MediaPlayer.Play(song);
                MediaPlayer.IsRepeating = repeat;
                return true;
            }
            return false;
        }

        public override void FixedUpdate(float step)
        {
        }

        public override void StandardUpdate(float dt)
        {
        }
    }
}
