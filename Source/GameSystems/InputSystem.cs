﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using AssGameFramework.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameSystems
{
    public class InputSystem : ISystem
    {
        public enum MouseButtons
        {
            Left,
            Right,
            Middle
        }

        public delegate void KeyboardKeyChangedDelegate(Keys keyPressed, bool state);
        public delegate void MouseMovedDelegate(Vector2 position, Vector2 delta);
        public delegate void MouseButtonChangedDelegate(MouseButtons button, bool state);
        public delegate void MouseWheelChangedDelegate(int position, int delta);

        public delegate void ControllerButtonChangedDelegate(PlayerIndex index, Buttons button, bool state);
        public delegate void ControllerAnalogChangedDelegate(PlayerIndex index, Buttons button, float value);
        public delegate void ThumbStickChangedDelegate(PlayerIndex index, Vector2 value);

        private KeyboardState _keyboardState;
        private MouseState _mouseState;
        private GamePadState[] _gamePadState = new GamePadState[4];

        private event KeyboardKeyChangedDelegate _KeyPressedEvent;
        private event KeyboardKeyChangedDelegate _KeyReleasedEvent;
        private event MouseMovedDelegate _mouseMovedEvent;
        private event MouseButtonChangedDelegate _mouseButtonPressedEvent;
        private event MouseButtonChangedDelegate _mouseButtonReleasedEvent;
        private event MouseWheelChangedDelegate _mouseWheelChangedEvent;

        private event ControllerButtonChangedDelegate _buttonPressedEvent;
        private event ControllerButtonChangedDelegate _buttonReleasedEvent;
        private event ControllerAnalogChangedDelegate _analogChangedEvent;
        private event ThumbStickChangedDelegate _leftThumbstickEvent;
        private event ThumbStickChangedDelegate _rightThumbstickEvent;


        public InputSystem()
        {
            _keyboardState = Keyboard.GetState();
            _mouseState = Mouse.GetState();
    }
        
        public void SubscribeToKeyPressedEvent(KeyboardKeyChangedDelegate keyDelegate)
        {
            _KeyPressedEvent += keyDelegate;
        }

        public void UnsubscribeToKeyPressedEvent(KeyboardKeyChangedDelegate keyDelegate)
        {
            _KeyPressedEvent -= keyDelegate;
        }

        public void SubscribeToKeyReleasedEvent(KeyboardKeyChangedDelegate keyDelegate)
        {
            _KeyReleasedEvent += keyDelegate;
        }

        public void UnsubscribeToKeyReleasedEvent(KeyboardKeyChangedDelegate keyDelegate)
        {
            _KeyReleasedEvent -= keyDelegate;
        }

        public void SubscribeToMouseMovedEvent(MouseMovedDelegate mouseDelegate)
        {
            _mouseMovedEvent += mouseDelegate;
        }

        public void SubscribeToGamePadButtonPressedEvent(ControllerButtonChangedDelegate butDelegate)
        {
            _buttonPressedEvent += butDelegate;
        }

        public void SubscribeToGamePadButtonReleasedEvent(ControllerButtonChangedDelegate butDelegate)
        {
            _buttonReleasedEvent += butDelegate;
        }

        public void SubscribeToGamePadAnalogChangedEvent(ControllerAnalogChangedDelegate analogDelegate)
        {
            _analogChangedEvent += analogDelegate;
        }

        public void UnsubscribeToMouseMovedEvent(MouseMovedDelegate mouseDelegate)
        {
            _mouseMovedEvent -= mouseDelegate;
        }

        public void SubscribeToMouseButtonPressedEvent(MouseButtonChangedDelegate mouseDelegate)
        {
            _mouseButtonPressedEvent += mouseDelegate;
        }

        public void UnsubscribeToMouseButtonPressedEvent(MouseButtonChangedDelegate mouseDelegate)
        {
            _mouseButtonPressedEvent -= mouseDelegate;
        }

        public void SubscribeToMouseButtonReleasedEvent(MouseButtonChangedDelegate mouseDelegate)
        {
            _mouseButtonReleasedEvent += mouseDelegate;
        }

        public void UnsubscribeToMouseButtonReleasedEvent(MouseButtonChangedDelegate mouseDelegate)
        {
            _mouseButtonReleasedEvent -= mouseDelegate;
        }

        public void SubscribeToMouseWheelChangedEvent(MouseWheelChangedDelegate mouseDelegate)
        {
            _mouseWheelChangedEvent += mouseDelegate;
        }

        public void UnsubscribeToMouseWheelChangedEvent(MouseWheelChangedDelegate mouseDelegate)
        {
            _mouseWheelChangedEvent -= mouseDelegate;
        }

        public void UnsubscribeToGamePadButtonPressedEvent(ControllerButtonChangedDelegate butDelegate)
        {
            _buttonPressedEvent -= butDelegate;
        }

        public void UnsubscribeToGamePadButtonReleasedEvent(ControllerButtonChangedDelegate butDelegate)
        {
            _buttonReleasedEvent -= butDelegate;
        }

        public void UnsubscribeToGamePadAnalogChangedEvent(ControllerAnalogChangedDelegate analogDelegate)
        {
            _analogChangedEvent -= analogDelegate;
        }

        public void SubscribeToGamePadThumbstickLeftEvent(ThumbStickChangedDelegate thumbDelegate)
        {
            _leftThumbstickEvent += thumbDelegate;
        }

        public void SubscribeToGamePadThubmstickRightEvent(ThumbStickChangedDelegate thumbDelegate)
        {
            _rightThumbstickEvent += thumbDelegate;
        }

        public void UnsubscribeToGamePadThumbstickLeftEvent(ThumbStickChangedDelegate thumbDelegate)
        {
            _leftThumbstickEvent -= thumbDelegate;
        }

        public void UnsubscribeToGamePadThubmstickRightEvent(ThumbStickChangedDelegate thumbDelegate)
        {
            _rightThumbstickEvent -= thumbDelegate;
        }

        public bool IsKeyPressed(Keys key)
        {
            return _keyboardState.IsKeyDown(key);
        }

        public Vector2 GetMousePosition()
        {
            return new Vector2(_mouseState.X, _mouseState.Y);
        }

        public bool IsMouseButtonPressed(MouseButtons mouseButton)
        {
            switch(mouseButton)
            {
                case MouseButtons.Left:
                    return _mouseState.LeftButton == ButtonState.Pressed;
                case MouseButtons.Right:
                    return _mouseState.RightButton == ButtonState.Pressed;
                case MouseButtons.Middle:
                    return _mouseState.MiddleButton == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public int GetMouseWheelPosition()
        {
            return _mouseState.ScrollWheelValue;
        }

        public override void FixedUpdate(float step)
        {
            
        }

        public override void StandardUpdate(float dt)
        {
            KeyboardState newKeyboardState = Keyboard.GetState();
            MouseState newMouseState = Mouse.GetState();

            if (_KeyReleasedEvent != null)
            {
                foreach (Keys key in _keyboardState.GetPressedKeys())
                {
                    if (newKeyboardState.IsKeyUp(key))
                    {
                        _KeyReleasedEvent?.Invoke(key, false);
                    }
                }
            }

            if (_KeyPressedEvent != null)
            {
                foreach (Keys key in newKeyboardState.GetPressedKeys())
                {
                    if (_keyboardState.IsKeyUp(key))
                    {
                        _KeyPressedEvent?.Invoke(key, true);
                    }
                }
            }

            if(_mouseMovedEvent != null && _mouseState.Position != newMouseState.Position)
            {
                Vector2 oldPos = new Vector2(_mouseState.X, _mouseState.Y);
                Vector2 newPos = new Vector2(newMouseState.X, newMouseState.Y);
                _mouseMovedEvent?.Invoke(newPos, oldPos - newPos);
            }

            if(_mouseButtonPressedEvent != null && _mouseState.LeftButton != newMouseState.LeftButton)
            {
                if (newMouseState.LeftButton == ButtonState.Pressed)
                {
                    _mouseButtonPressedEvent?.Invoke(MouseButtons.Left, true);
                }
                else
                {
                    _mouseButtonReleasedEvent?.Invoke(MouseButtons.Left, false);
                }
            }

            if(_mouseButtonPressedEvent != null && _mouseState.RightButton != newMouseState.RightButton)
            {
                if (newMouseState.RightButton == ButtonState.Pressed)
                {
                    _mouseButtonPressedEvent?.Invoke(MouseButtons.Right, true);
                }
                else
                {
                    _mouseButtonReleasedEvent?.Invoke(MouseButtons.Right, false);
                }
            }

            if (_mouseButtonPressedEvent != null && _mouseState.MiddleButton != newMouseState.MiddleButton)
            {
                if (newMouseState.MiddleButton == ButtonState.Pressed)
                {
                    _mouseButtonPressedEvent?.Invoke(MouseButtons.Middle, true);
                }
                else
                {
                    _mouseButtonReleasedEvent?.Invoke(MouseButtons.Middle, false);
                }
            }

            if (_mouseWheelChangedEvent != null && _mouseState.ScrollWheelValue != newMouseState.ScrollWheelValue)
            {
                _mouseWheelChangedEvent?.Invoke(newMouseState.ScrollWheelValue, _mouseState.ScrollWheelValue - newMouseState.ScrollWheelValue);
            }

            _keyboardState = newKeyboardState;
            _mouseState = newMouseState;

            for (int i = 0; i < 4; ++i)
            {
                GamePadState newGamePadState = GamePad.GetState(i);
                if (newGamePadState.IsConnected)
                { 
                    GamePadState oldGamePadState = _gamePadState[i];

                    if (_buttonPressedEvent != null || _buttonReleasedEvent != null)
                    {
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.A);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.B);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.X);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.Y);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.Back);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.Start);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.DPadUp);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.DPadDown);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.DPadLeft);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.DPadRight);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.LeftShoulder);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.RightShoulder);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.LeftStick);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.RightStick);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.BigButton);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.LeftTrigger);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.RightTrigger);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.LeftThumbstickUp);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.LeftThumbstickDown);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.LeftThumbstickLeft);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.LeftThumbstickRight);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.RightThumbstickUp);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.RightThumbstickDown);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.RightThumbstickLeft);
                        TestAndInvokeGamePadEvents((PlayerIndex)i, newGamePadState, oldGamePadState, Buttons.RightThumbstickRight);
                    }

                    TestAndInvokeAnalogEvents((PlayerIndex)i, newGamePadState, oldGamePadState);

                    TestAndInvokeThumbstickEvents((PlayerIndex)i, newGamePadState, oldGamePadState);
                }
                _gamePadState[i] = newGamePadState;
            }
        }
        
        private void TestAndInvokeThumbstickEvents(PlayerIndex index, GamePadState newState, GamePadState oldState)
        {
            if (_leftThumbstickEvent != null)
            {
                if (newState.ThumbSticks.Left != oldState.ThumbSticks.Left)
                {
                    _leftThumbstickEvent.Invoke(index, newState.ThumbSticks.Left);
                }
            }

            if (_rightThumbstickEvent != null)
            {
                if (newState.ThumbSticks.Right != oldState.ThumbSticks.Right)
                {
                    _rightThumbstickEvent.Invoke(index, newState.ThumbSticks.Right);
                }
            }

        }

        private void TestAndInvokeAnalogEvents(PlayerIndex index, GamePadState newState, GamePadState oldState)
        {
            if (_analogChangedEvent != null)
            {
                if (newState.Triggers.Left != oldState.Triggers.Left)
                {
                    _analogChangedEvent.Invoke(index, Buttons.LeftTrigger, newState.ThumbSticks.Left.X);
                }
                if (newState.Triggers.Right != oldState.Triggers.Right)
                {
                    _analogChangedEvent.Invoke(index, Buttons.RightTrigger, newState.ThumbSticks.Right.X);
                }
            }
        }

        private void TestAndInvokeGamePadEvents(PlayerIndex index, GamePadState newState, GamePadState oldState, Buttons button)
        {
            if (_buttonPressedEvent != null && TestGamePadChanged(newState, oldState, button) > 0)
            {
                _buttonPressedEvent.Invoke(index, button, true);
            }
            else if (_buttonReleasedEvent != null && TestGamePadChanged(newState, oldState, button) < 0)
            {
                _buttonReleasedEvent(index, button, false);
            }
        }

        private int TestGamePadChanged(GamePadState newState, GamePadState oldState, Buttons button)
        {
            if (newState.IsButtonUp(button))
            {
                if (oldState.IsButtonUp(button))
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (oldState.IsButtonUp(button))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
