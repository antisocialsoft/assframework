﻿using AssGameFramework.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.UI
{
    public class UIImageComponent : UIComponent
    {
        public Texture2D Image { get; set; }
        public Color Color { get; set; } = Color.White;

        public UIImageComponent(Texture2D image)
        {
            Image = image;
        }

        public override void Draw(float dt, SpriteBatch spriteBatch)
        {
            if(Image != null)
            {
                Rectangle destRect = new Rectangle((int)Owner.Position.X, (int)Owner.Position.Y, (int)Owner.Size.X, (int)Owner.Size.Y);
                spriteBatch.Draw(Image, destRect, Color);
            }
        }
    }
}
