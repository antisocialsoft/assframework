﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AssGameFramework.UI
{
    public class UITextComponent : UIComponent
    {
        public SpriteFont TextFont { get; set; }
        public virtual String Text { get; set; }
        public Color TextColour { get; set; }

        public UITextComponent()
        {
            TextColour = Color.Black;
        }

        public override void Draw(float dt, SpriteBatch spriteBatch)
        {
            if (TextFont != null)
            {
                Vector2 position = Owner.Position;
                spriteBatch.DrawString(TextFont, Text, position, TextColour);
            }
        }
    }
}
