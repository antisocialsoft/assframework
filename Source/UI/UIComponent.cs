﻿using AssGameFramework.GameSystems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AssGameFramework.UI.UIEntity;

namespace AssGameFramework.UI
{
    public abstract class UIComponent
    {
        public UIEntity Owner { get; private set; }
        public virtual bool Focus { get; protected set; }
        public DepthStencilState StencilState { get; set; } = null;
        public AlphaTestEffect AlphaTest { get; set; }

        public virtual void StandardUpdate(float dt) { }
        public virtual void Draw(float dt, SpriteBatch spriteBatch) { }
        
        public UIComponent()
        {
            Focus = false;
        }

        internal void AddToEntity(UIEntity newParent)
        {
            bool validParent = (Owner != null) && (Owner != newParent);

            if (validParent)
            {
                RemoveFromEntity();
            }

            validParent = (Owner == null) || Owner != newParent;

            Owner = newParent;

            Owner.SubscribeToFocusChangedEvent(new FocusChangedDelegate(OnFocusChanged));
        }

        private void OnFocusChanged(bool state)
        {
            Focus = state;
        }

        internal void RemoveFromEntity()
        {
            if (Owner != null)
            {
                Owner.UnsubscribeToFocusChangedEvent(new FocusChangedDelegate(OnFocusChanged));
                Owner = null;
            }
        }
    }
}
