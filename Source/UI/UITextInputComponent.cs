﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Core;
using AssGameFramework.GameSystems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AssGameFramework.UI
{
    public class UITextInputComponent : UITextComponent
    {
        float _flashTime = 0.2f;
        float _currentFlash = 0.0f;
        bool _showingCursor = false;

        int _cursorPos = -1;

        bool _updating = false;

        public override bool Focus
        {
            get => base.Focus;
            protected set
            {
                if (!value) _showingCursor = false;
                base.Focus = value;
            }
        }

        public override string Text { get => base.Text;
            set {
                base.Text = value;
                if(!_updating && _cursorPos >= 0)
                {
                    ++_cursorPos;
                }
            } }

        public List<Keys> AllowedKeys { get; set; }

        public UITextInputComponent()
        {
            AllowedKeys = new List<Keys>();

            for(int i = (int)Keys.A; i <= (int)Keys.Z; ++i)
            {
                AllowedKeys.Add((Keys)i);
            }

            for (int i = (int)Keys.D0; i <= (int)Keys.D9; ++i)
            {
                AllowedKeys.Add((Keys)i);
            }

            AllowedKeys.Add(Keys.Divide);
            AllowedKeys.Add(Keys.OemBackslash);

            InputSystem inputSys = ApplicationContext.Instance.GetApplicationSystem<InputSystem>();

            if(inputSys != null)
            {
                //inputSys.SubscribeToKeyPressedEvent(new InputSystem.KeyboardKeyChangedDelegate(ButtonChanged));
                //inputSys.SubscribeToKeyReleasedEvent(new InputSystem.KeyboardKeyChangedDelegate(ButtonChanged));
            }

            ApplicationContext.Instance.TextInputEvent += new EventHandler<TextInputEventArgs>(OnTextInput);
        }

        private void OnTextInput(object sender, TextInputEventArgs e)
        {
            //Easy out if we're not in focus
            if (!Focus)
                return;

            int insertPos = (_cursorPos < 0) ? Text.Length : _cursorPos;
            if (e.Character >= 32)
            {
                Text = Text.Insert(insertPos, e.Character.ToString());
            }
            else
            {
                //TODO: Handle special characters
                switch(e.Key)
                {
                    case Keys.Delete:
                    case Keys.Back:
                        if(Text.Length > 0)
                            Text = Text.Remove(Text.Length - 1);
                        break;
                    case Keys.Left:
                        if (_cursorPos == -1)
                        {
                            _cursorPos = Text.Length - 1;
                        }
                        else
                        {
                            _cursorPos -= 1;
                            Math.Max(_cursorPos, 0);
                        }
                        break;
                    case Keys.Right:
                        _cursorPos += 1;
                        if (_cursorPos >= Text.Length)
                        {
                            _cursorPos = -1;
                        }
                        break;
                    case Keys.Enter:
                        Focus = false;
                        break;
                }
            }
        }

        ~UITextInputComponent()
        {
            InputSystem inputSys = ApplicationContext.Instance.GetApplicationSystem<InputSystem>();

            if (inputSys != null)
            {
                //inputSys.UnsubscribeToKeyPressedEvent(new InputSystem.KeyboardKeyChangedDelegate(ButtonChanged));
                //inputSys.UnsubscribeToKeyReleasedEvent(new InputSystem.KeyboardKeyChangedDelegate(ButtonChanged));
            }
        }

        public virtual void ButtonChanged(Keys key, bool pressedState)
        {
            //if(AllowedKeys.Contains(key))
            //{
            //    int insertPos = (_cursorPos < 0)? Text.Length: _cursorPos;
            //    String toInsert = ("" + (char)key);

            //    InputSystem inputSys = ApplicationContext.Instance.GetApplicationSystem<InputSystem>();
            //    if (inputSys != null && !(inputSys.IsKeyPressed(Keys.LeftShift) || inputSys.IsKeyPressed(Keys.RightShift)))
            //    {
            //        toInsert = toInsert.ToLower();
            //    }

            //    Text = Text.Insert(insertPos, toInsert);
            //}
            int insertPos = (_cursorPos < 0)? Text.Length: _cursorPos;
            Text = Text.Insert(insertPos, key.ToString());

            switch(key)
            {
                case Keys.Delete:
                case Keys.Back:
                   Text = Text.Remove(Text.Length - 1);
                    break;
                case Keys.Left:
                    if (_cursorPos == -1)
                    {
                        _cursorPos = Text.Length - 1;
                    }
                    else
                    {
                        _cursorPos -= 1;
                        Math.Max(_cursorPos, 0);
                    }
                    break;
                case Keys.Right:
                        _cursorPos += 1;
                        if(_cursorPos >= Text.Length)
                        {
                            _cursorPos = -1;
                        }
                    break;

            }
        }
        
        public override void Draw(float dt, SpriteBatch spriteBatch)
        {
            if (Focus)
            {
                _currentFlash += dt;
                if (_currentFlash > _flashTime)
                {
                    _currentFlash -= _flashTime;
                    _showingCursor = !_showingCursor;
                }
            }

            _updating = true;
            String originalText = Text;
            int cursorPos = (_cursorPos < 0) ? Text.Length : _cursorPos;
            Text = Text.Insert(cursorPos, (_showingCursor) ? "|" : " ");
            base.Draw(dt, spriteBatch);
            Text = originalText;
            _updating = false;
        }
    }
}
