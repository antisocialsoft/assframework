﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Core;
using AssGameFramework.GameSystems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AssGameFramework.UI
{
    public class UIScene
    {
        public List<UIEntity> Entities
        {
            get;
            private set;
        }

        public bool HasCustomSize
        { get; private set; }

        private Vector2 _customSize = new Vector2();

        public Vector2 Size {
            get
            {
                if(HasCustomSize)
                {
                    return _customSize;
                }
                else
                {
                    Viewport viewport = ApplicationContext.Instance.GraphicsManager.GraphicsDevice.Viewport;
                    return new Vector2(viewport.Width, viewport.Height);
                }
            }
            set
            {
                _customSize = value;
                HasCustomSize = true;
            }
        }

        public UIScene()
        {
            Entities = new List<UIEntity>();

            InputSystem input = ApplicationContext.Instance.GetApplicationSystem<InputSystem>();

            if(input != null)
            {
                input.SubscribeToMouseButtonPressedEvent(MouseButtonPressed);
                input.SubscribeToMouseButtonReleasedEvent(MouseButtonReleased);
            }
        }

        ~UIScene()
        {
            InputSystem input = ApplicationContext.Instance.GetApplicationSystem<InputSystem>();

            if (input != null)
            {
                input.UnsubscribeToMouseButtonPressedEvent(MouseButtonPressed);
                input.UnsubscribeToMouseButtonReleasedEvent(MouseButtonReleased);
            }
        }


        private void MouseButtonReleased(InputSystem.MouseButtons button, bool state)
        {
            foreach (UIEntity ent in Entities)
            {
                InputSystem input = ApplicationContext.Instance.GetApplicationSystem<InputSystem>();
                Vector2 mousePos = input.GetMousePosition();
                ent.OnMouseReleased(button, mousePos);
            }
        }

        private void MouseButtonPressed(InputSystem.MouseButtons button, bool state)
        {
            foreach (UIEntity ent in Entities)
            {
                InputSystem input = ApplicationContext.Instance.GetApplicationSystem<InputSystem>();
                Vector2 mousePos = input.GetMousePosition();
                ent.OnMousePressed(button, mousePos);
            }
        }

        public void AddEntity(UIEntity entity)
        {
            entity.SetScene(this);
            Entities.Add(entity);
        }

        public void RemoveEntity(UIEntity entity)
        {
                Entities.Remove(entity);
                entity.SetScene(null);
        }

        public UIEntity GetEntity(String entityName)
        {
            Predicate<UIEntity> search = delegate (UIEntity compare)
            {
                return (compare.Name == entityName);
            };

            UIEntity foundEnt = Entities.Find(search);

            return foundEnt;
        }

        public UIEntity GetEntity(uint entityID)
        {
            Predicate<UIEntity> search = delegate (UIEntity compare)
            {
                return (compare.EntityID == entityID);
            };

        UIEntity foundEnt = Entities.Find(search);

            return foundEnt;
        }

        public void Draw(float dt, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            foreach (UIEntity ent in Entities)
            {
                if (ent.Visible)
                {
                    DrawEnt(ent, dt, spriteBatch);
                }
            }
            spriteBatch.End();
        }

        private Stack<DepthStencilState> stencilStates = new Stack<DepthStencilState>();

        private void DrawEnt(UIEntity ent, float dt, SpriteBatch spriteBatch)
        {
            bool changedState = false;
            foreach (UIComponent comp in ent.GetComponents<UIComponent>())
            {
                if(comp.StencilState != null)
                {
                    spriteBatch.End();
                    spriteBatch.Begin(sortMode: SpriteSortMode.Immediate, depthStencilState: comp.StencilState, effect: comp.AlphaTest);
                    stencilStates.Push(comp.StencilState);
                    changedState = true;
                }
                comp.Draw(dt, spriteBatch);
            }

            foreach (UIEntity child in ent.Children)
            {
                DrawEnt(child, dt, spriteBatch);
            }

            if(changedState)
            {
                stencilStates.Pop();
                spriteBatch.End();
                if (stencilStates.Count > 0)
                {
                    spriteBatch.Begin(depthStencilState: stencilStates.Peek());
                }
                else
                {
                    spriteBatch.Begin();
                }
            }
        }
    }
}
