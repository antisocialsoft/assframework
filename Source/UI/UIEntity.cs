﻿using AssGameFramework.Core;
using AssGameFramework.GameSystems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.UI
{
    public class UIEntity
    {
        private class UIEntityStatusPair
        {
            public UIEntityStatusPair(UIEntity ent) { _ent = ent; }

            public UIEntity _ent;
            public bool _toRemove;
        };

        private class UIComponentStatusPair
        {
            public UIComponentStatusPair(UIComponent comp) { _comp = comp; }

            public UIComponent _comp;
            public bool _toRemove;
        };

        private bool _selected = false;
        public bool Selected {
            get
            {
                return _selected;
            }
            set
            {
                if (_focusChangedEvent != null && value != _selected)
                {
                    _selected = value;
                    _focusChangedEvent.Invoke(value);
                }
                _selected = value;
            }
        }

        public bool Visible { get; set; } = true;

        public delegate void FocusChangedDelegate(bool state);
        private event FocusChangedDelegate _focusChangedEvent;

        public void SubscribeToFocusChangedEvent(FocusChangedDelegate del) { _focusChangedEvent += del; }
        public void UnsubscribeToFocusChangedEvent(FocusChangedDelegate del) { _focusChangedEvent -= del; }

        public UIScene SceneOwner { get; private set; }
        static private uint entityCount = 0;
        private List<UIEntityStatusPair> _children;
        private List<UIComponentStatusPair> _components;

        public List<UIEntity> Children
        {
            get
            {
                List<UIEntity> children = new List<UIEntity>();
                foreach (UIEntityStatusPair child in _children)
                {
                    if (child._toRemove == false)
                    {
                        children.Add(child._ent);
                    }
                }
                return children;
            }
        }

        public uint EntityID { get; private set; }

        public String Name { get; set; }

        public Vector2 AbsolutePosition { get; set; }
        public Vector2 RelativePosition { get; set; }
        public Vector2 AbsoluteSize { get; set; }
        public Vector2 RelativeSize { get; set; }
        public float Rotation { get; set; }
        public Vector2 LocalScale { get; set; } = new Vector2(1.0f, 1.0f);
        public Vector2 Scale
        {
            get
            {
                if(Parent != null)
                {
                    return Parent.Scale * LocalScale;
                }
                else
                {
                    return LocalScale;
                }
            }
        }

        public UIEntity Parent { get; private set; }

        public Vector2 Position
        {
            get
            {
                Vector2 parentSize = new Vector2();
                Vector2 parentPos = new Vector2();

                if (Parent != null)
                {
                    parentPos = Parent.Position;
                    parentSize = Parent.Size;
                }
                else if (SceneOwner != null)
                {
                    parentSize = SceneOwner.Size;
                }

                return parentPos + ((parentSize * RelativePosition) + AbsolutePosition);
            }
        }

        public Vector2 Size {
            get
            {
                if (RelativeSize != Vector2.Zero)
                {
                    Vector2 parentSize = new Vector2();

                    if (Parent != null)
                    {
                        parentSize = Parent.Size;
                    }
                    else if (SceneOwner != null)
                    {
                        parentSize = SceneOwner.Size;
                    }

                    return ((parentSize * RelativeSize) + AbsoluteSize) * Scale;
                }
                else
                {
                    return AbsoluteSize * Scale;
                }
            }
            }

        public UIEntity()
        {
            _children = new List<UIEntityStatusPair>();
            _components = new List<UIComponentStatusPair>();

            EntityID = entityCount++;
            Name = String.Format("Entity_{0:D3}", EntityID);
        }

        internal void OnMousePressed(InputSystem.MouseButtons button, Vector2 mousePos)
        {
            Vector2 size = Size;
            Vector2 pos = Position;

            if (mousePos.X > pos.X && mousePos.X < size.X + pos.X &&
                mousePos.Y > pos.Y && mousePos.Y < size.Y + pos.Y)
            {
                Selected = true;
            }
            else
            {
                Selected = false;
            }
        }

        internal void OnMouseReleased(InputSystem.MouseButtons button, Vector2 mousePos)
        {
            Vector2 size = Size;
            Vector2 pos = Position;

            if (mousePos.X > pos.X && mousePos.X < size.X + pos.X &&
                mousePos.Y > pos.Y && mousePos.Y < size.Y + pos.Y)
            {
                Selected = true;
            }
            else
            {
                Selected = false;
            }
        }

        public void SetScene(UIScene scene)
        {
            if (scene != SceneOwner)
            {
                SceneOwner = scene;
            }
        }

        public void AddChildEntity(UIEntity newChild)
        {
            _children.Add(new UIEntityStatusPair(newChild));

            newChild.AddToParent(this);
            newChild.SetScene(SceneOwner);
        }

        public void RemoveChildEntity(UIEntity child)
        {
            Predicate<UIEntityStatusPair> search = delegate (UIEntityStatusPair compare)
            {
                return (compare._ent == child && compare._toRemove == false);
            };

            UIEntityStatusPair status = _children.Find(search);

            if (status != null && status._toRemove == false)
            {
                status._toRemove = true;

                child.RemoveFromParent();
                child.SetScene(null);
            }
        }

        private void RemoveFromParent()
        {
            if (Parent != null)
            {
                Parent.RemoveChildEntity(this);
                Parent = null;
            }
        }

        private void AddToParent(UIEntity newParent)
        {
            Debug.Assert(newParent != null, "Cannot add to null. Did you mean to call RemoveFromParent?");

            if (Parent != null && Parent != newParent)
            {
                Parent.RemoveChildEntity(this);
            }

            Parent = newParent;
        }

        public void Draw(float dt, SpriteBatch spriteBatch)
        {
            if(!Visible)
            {
                return;
            }

            for (int i = 0; i < _components.Count; ++i)
            {
                UIComponentStatusPair comp = _components[i];
                comp._comp.Draw(dt, spriteBatch);
            }

            for (int i = 0; i < _children.Count; ++i)
            {
                if (_children[i]._toRemove == false)
                {
                    UIEntity child = _children[i]._ent;
                    child.Draw(dt, spriteBatch);
                }
            }
        }

        public void AddComponent(UIComponent entComp)
        {
            _components.Add(new UIComponentStatusPair(entComp));

            entComp.AddToEntity(this);
        }

        public void RemoveComponent(UIComponent entComp)
        {
            entComp.RemoveFromEntity();

            for(int i = 0; i < _components.Count; ++i)
            {
                if(_components[i]._toRemove == false && _components[i]._comp == entComp)
                {
                    _components.RemoveAt(i);
                }
            }
        }

        /**
        * GetComponent T
        * 
        * Finds the first T in the component list and returns it
        * 
        **/
        public T GetComponent<T>() where T : UIComponent
        {
            foreach (UIComponentStatusPair comp in _components)
            {
                if (comp._comp is T && comp._toRemove == false)
                {
                    return (T)comp._comp;
                }
            }

            return null;
        }

        /**
         * Get Component Recursive T
         * 
         * Finds the first T in the component list and returns it
         * If no T is found, recurse through the children.
         * 
         **/
        public T GetComponentRecursive<T>() where T : UIComponent
        {
            foreach (UIComponentStatusPair comp in _components)
            {
                if (comp._comp is T && comp._toRemove == false)
                {
                    return (T)comp._comp;
                }
            }

            foreach (UIEntityStatusPair child in _children)
            {
                if (child._toRemove == false)
                {
                    UIComponent comp = child._ent.GetComponentRecursive<T>();
                    if (comp != null)
                    {
                        return (T)comp;
                    }
                }
            }

            return null;
        }

        /**
        * Get Components T
        * 
        * Gets all T components and returns them as a list
        * 
        **/
        public List<T> GetComponents<T>() where T : UIComponent
        {
            Predicate<UIComponentStatusPair> search = delegate (UIComponentStatusPair compare)
            {
                return (compare._comp is T && compare._toRemove == false);
            };

            List<UIComponentStatusPair> pairs = _components.FindAll(search);
            List<T> list = new List<T>();

            foreach (UIComponentStatusPair pair in pairs)
            {
                list.Add((T)(pair._comp));
            }

            return list;
        }

        /**
        * Get Components Recursive T
        * 
        * Gets all T components in component list and children's components lists
        * 
        **/
        public List<T> GetComponentsRecursive<T>() where T : UIComponent
        {
            List<T> comps = GetComponents<T>();
            foreach (UIEntityStatusPair child in _children)
            {
                if (child._toRemove == false)
                {
                    comps.AddRange(child._ent.GetComponentsRecursive<T>());
                }
            }

            return comps;
        }

        public UIEntity GetChildEntity(String entityName, bool recursive)
        {
            Predicate<UIEntityStatusPair> search = delegate (UIEntityStatusPair compare)
            {
                return (compare._ent.Name == entityName && compare._toRemove == false);
            };

            UIEntityStatusPair status = _children.Find(search);

            if (status != null)
            {
                return status._ent;
            }
            else if (recursive)
            {
                foreach (UIEntityStatusPair childStatus in _children)
                {
                    if (childStatus._toRemove == false)
                    {
                        UIEntity foundEnt = childStatus._ent.GetChildEntity(entityName, recursive);
                        if (foundEnt != null)
                        {
                            return foundEnt;
                        }
                    }
                }
            }

            return null;
        }
    }
}
