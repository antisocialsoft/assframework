﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.StateSystem
{
    public class StateMachine
    {
        protected Stack<State> _activeStates = new Stack<State>();

        /**
         * Get the top of the state stack.
         **/
        public State ActiveState
        {
            get
            {
                Debug.Assert(_activeStates.Count > 0, "Stack is empty.");
                return _activeStates.Peek();
            }
        }

        public bool EmptyStack
        {
            get
            {
                return _activeStates.Count == 0;
            }
        }


        private enum StateActions
        {
            STATE_ACTION_PUSH,
            STATE_ACTION_POP,
            STATE_ACTION_CHANGE
        };

        private struct ActionCommand
        {
            public ActionCommand(StateActions stateAction, State actionState)
            {
                action = stateAction;
                state = actionState;
            }
            public StateActions action;
            public State state;
        };

        private List<ActionCommand> actionQueue;

        /**
         * Constructor
         **/
        public StateMachine()
        {
            actionQueue = new List<ActionCommand>();
        }


        /**
         * Update State Machine
         * 
         * Process the cached actions from the last frame
         **/ 
        public void UpdateStateMachine()
        {
            //For each command in the command queue
            foreach (ActionCommand command in actionQueue)
            {
                switch(command.action)
                {
                    case StateActions.STATE_ACTION_PUSH:
                        {
                            //Push a new state
                            if(!EmptyStack)
                            {
                                State oldState = ActiveState;
                                oldState.InactiveEvent();
                            }
                            command.state.OnCreate();
                            _activeStates.Push(command.state);
                            command.state.ActiveEvent();
                        }
                        break;
                    case StateActions.STATE_ACTION_POP:
                        {
                            Debug.Assert(_activeStates.Count > 0, "Stack is empty. Cannot pop state.");
                            //Pop the top state
                            State oldState = ActiveState;
                            oldState.InactiveEvent();
                            _activeStates.Pop();
                            oldState.OnDestroy();
                            if (!EmptyStack)
                            {
                                ActiveState.ActiveEvent();
                            }
                        }
                        break;
                    case StateActions.STATE_ACTION_CHANGE:
                        {
                            //Pop the current state and push a new state on top
                            State oldState = ActiveState;
                            oldState.InactiveEvent();
                            _activeStates.Pop();
                            oldState.OnDestroy();
                            command.state.OnCreate();
                            _activeStates.Push(command.state);
                            command.state.ActiveEvent();
                        }
                        break;
                    default:
                        Debug.Assert(false, "Invalid command");
                        break;
                }
            }

            actionQueue.Clear();
        }

        /**
         * Push State
         * 
         * Pushes a new state onto the stack to become the active state.
         **/
        public void PushState(State newState)
        {
            actionQueue.Add(new ActionCommand(StateActions.STATE_ACTION_PUSH, newState));
        }

        /**
         * Pop State
         * 
         * Removes the active state from the stack. 
         * The state under this one will become active, if there is one.
        **/
        public void PopState()
        {
            actionQueue.Add(new ActionCommand(StateActions.STATE_ACTION_POP, null));
        }

        /**
         * Change State
         * 
         * A pop and push action in one. Will pop the top state and replace it with a new one.
        **/
        public void ChangeState(State newState)
        {
            actionQueue.Add(new ActionCommand(StateActions.STATE_ACTION_CHANGE, newState));
        }
    }
}
