﻿using Microsoft.Xna.Framework.Graphics;
using AssGameFramework.SceneGraph;
using AssGameFramework.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using AssGameFramework.Core;
using Microsoft.Xna.Framework;
using AssGameFramework.UI;

namespace AssGameFramework.StateSystem
{
    public abstract class State
    {
        private HashSet<ISystem> _systems;

        //The scene associated with the state
        public Scene StateScene { get; protected set; }
        public UIScene UICanvas { get; protected set; }
        protected ApplicationContext Context { get; private set; }

        /**
         * Constructor
         **/ 
        public State(ApplicationContext context)
        {
            StateScene = new Scene();
            _systems = new HashSet<ISystem>();
            Context = context;
            UICanvas = new UIScene();
        }

        /**
         * Standard Update
         * 
         * Updates each frame with a variable delta time.
         * 
         * dt : time since last frame in seconds
         */ 
        public virtual void StandardUpdate(float dt)
        {
            StateScene.StandardUpdate(dt);
            foreach (ISystem sys in _systems)
            {
                sys.StandardUpdate(dt);
            }
        }

        /**
         * Fixed Update
         * 
         * Fixed update every step.
         * 
         * step: Fixed value time step in seconds
         */
        public virtual void FixedUpdate(float step)
        {
            StateScene.FixedUpdate(step);
            foreach (ISystem sys in _systems)
            {
                sys.FixedUpdate(step);
            }
        }

        /**
         * Late Update
         * 
         * Update after systems, such as physics.
         * 
         * dt: Time since last frame in seconds
         */ 
        public virtual void LateUpdate(float dt)
        {
            StateScene.LateUpdate(dt);
        }

        /**
         * Late Fixed Update
         * 
         * Update on a fixed time step after systems.
         * 
         * step: Fixed value time step in seconds
         */
        public virtual void LateFixedUpdate(float step)
        {
            StateScene.LateFixedUpdate(step);
        }

        /**
         * Draw
         * 
         * Draw the scene graph.
         * 
         * dt: Seconds since last frame
         * spriteBatch: Sprite Batch object to draw with
         */
        public virtual void Draw(float dt, SpriteBatch spriteBatch)
        {
            StateScene.Draw(dt, spriteBatch);
            UICanvas.Draw(dt, spriteBatch);
        }

        /**
         * Get State System
         * 
         * Gets the state system of the given type.
         * 
         */ 
        public T GetStateSystem<T>() where T : ISystem
        {
            foreach (ISystem sys in _systems)
            {
                if (sys is T)
                {
                    return (T)sys;
                }
            }

            return null;
        }

        /**
         * Create
         * 
         * Called when added to the state machine.
         * 
         */
        protected abstract void Create();

        /**
         * Create State System
         * 
         * Create the systems for this state.
         * 
         */ 
        protected abstract void CreateStateSystems();

        /**
         * Add State System
         * 
         * Add a state system to the set of state systems
         */ 
        protected void AddStateSystem(ISystem system)
        {
            _systems.Add(system);
        }

        /**
         * Destroy
         * 
         * Called when removed from the state machine.
         * 
         */
        protected abstract void Destroy();

        /** On Inactive
         * 
         * Called when another state becomes active over this one.
         */
        protected abstract void OnInactive();

        /**
         * On Active
         * 
         * Called when the state is the top state on the state machine stack.
         */
        protected abstract void OnActive();

        //Internal method for calling On Inactive
        internal void InactiveEvent()
        {
            OnInactive();
        }

        //Internal method for calling On Active
        internal void ActiveEvent()
        {
            OnActive();
        }

        //Internal method for calling Create
        internal void OnCreate()
        {
            CreateStateSystems();
            Create();
        }

        //Internal method for calling Destroy
        internal void OnDestroy()
        {
            Destroy();
        }
    }
}
