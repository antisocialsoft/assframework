﻿using AssGameFramework.Core;
using AssGameFramework.SceneGraph;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameObjects
{
    public class Camera2DComponent : EntityComponent
    {

        public Matrix ViewMatrix
        {
            get
            {
                Viewport viewport = ApplicationContext.Instance.GraphicsManager.GraphicsDevice.Viewport;
                Vector3 halfsize = new Vector3(viewport.Width / 2.0f, viewport.Height / 2.0f, 0.0f);
                Matrix view = Matrix.CreateTranslation(halfsize - (Entity.WorldPosition3D));
                Matrix scale = Matrix.CreateScale(Entity.WorldScale3D);

                return scale*view;
            }
        }

        public Matrix Projection
        {
            get
            {
                float width = (ApplicationContext.Instance.GraphicsManager.PreferredBackBufferWidth / Entity.WorldScale.X);
                float height = (ApplicationContext.Instance.GraphicsManager.PreferredBackBufferHeight / Entity.WorldScale.Y);
                Vector2 cameraPos = Entity.WorldPosition;
                Matrix projection = Matrix.CreateOrthographicOffCenter(cameraPos.X - (width / 2),
                                                                    cameraPos.X  + (width / 2),
                                                                    cameraPos.Y + (height / 2),
                                                                    cameraPos.Y - (height / 2),
                                                                    0.1f, 100.0f);

                return projection;
            }
        }

    }
}
