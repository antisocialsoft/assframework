﻿using FarseerPhysics.Dynamics;
using AssGameFramework.SceneGraph;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameObjects
{
    public class CollisionComponent : EntityComponent
    {
        protected WeakReference<PhysicsComponent> _physComp = new WeakReference<PhysicsComponent>(null);

        public PhysicsComponent PhysComp
        {
            get
            {
                PhysicsComponent comp;
                _physComp.TryGetTarget(out comp);

                return comp;
            }
        }

        public CollisionComponent(PhysicsComponent physComp)
        {
            _physComp.SetTarget(physComp);
        }

        public virtual bool OnCollisionStart(Entity collidedWith)
        {
            return true;
        }

        public virtual bool OnCollisionEnded(Entity collidedWith)
        {
            return true;
        }

        public virtual bool OnNearMiss(Entity collidedWith)
        {
            return true;
        }

        protected override void OnAddToEntity(Entity newParent)
        {
        }

        protected override void OnRemoveFromEntity()
        {
            //Find all the bodies and fixtures and add ourselves as the user data for collisions
            PhysicsComponent physicsComp = Entity.GetComponent<PhysicsComponent>();
            if (physicsComp != null)
            {
                Body body = physicsComp.PhysicsBody;
                body.UserData = null;
                foreach (Fixture fix in body.FixtureList)
                {
                    fix.UserData = null;
                }
            }
        }
    }
}
