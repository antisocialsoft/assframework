﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using AssGameFramework.SceneGraph;
using SpriteSheetContentLoader;
using System;
using System.Diagnostics;
using static SpriteSheetContentLoader.SpriteSheet;

namespace AssGameFramework.GameObjects
{
    public class SpriteComponent : DrawableComponent
    {
        public SpriteEffects SpriteEffect { get; set; }
        public SpriteSheet SpriteSheet { get; set; }
        public Color SpriteColour { get; set; }

        public Vector2 SpritePivot { get; set; } = new Vector2(0.5f, 0.5f);
        public Point SpriteSize {
            get
            {
                return SpriteSheet.GetFrameDefinition(_currentFrame).Rect.Size;
            }
        }

        private int _currentFrame = 0;

        

        public SpriteComponent(Texture2D tex)
        {
            SpriteSheet = new SpriteSheet(tex);
            SpriteSheet.AddFrameDefinition("default", 0,0, tex.Width, tex.Height);
            SpriteColour = Color.White;
        }


        public SpriteComponent(SpriteSheet spriteSheet, String frameName)
        {
            SpriteSheet = spriteSheet;
            SetSpriteFrame(spriteSheet.GetIndexForFrameName(frameName));
        }

        public SpriteComponent(SpriteSheet spriteSheet, int frameNumber)
        {
            SpriteSheet = spriteSheet;
            SetSpriteFrame(frameNumber);
        }

        public void SetSpriteFrame(String spriteFrame)
        {
            SetSpriteFrame(SpriteSheet.GetIndexForFrameName(spriteFrame));
        }

        public void SetSpriteFrame(int spriteFrame)
        {
            Debug.Assert(spriteFrame >= 0, "Invalid sprite frame.");
            _currentFrame = spriteFrame;
        }

        public virtual Rectangle GetRectangle()
        {
            FrameDefinition frame = SpriteSheet.GetFrameDefinition(_currentFrame);
            return frame.Rect;
        }

        public Vector2 GetScale()
        {
            return Entity.WorldScale;
        }

        public override void Draw(float dt, SpriteBatch spriteBatch)
        {
            if(!Enabled)
            {
                return;
            }
            Vector2 pos = Entity.WorldPosition;
            Vector2 scale = Entity.WorldScale;
            float rotation = Entity.WorldRotation;

            Vector2 spriteScale = new Vector2(Math.Abs(scale.X), Math.Abs(scale.Y));

            FrameDefinition frame = SpriteSheet.GetFrameDefinition(_currentFrame);

            Vector2 origin = new Vector2(frame.Rect.Width * SpritePivot.X, frame.Rect.Height * SpritePivot.Y);

            SpriteEffects effect = SpriteEffect;

            //if (scale.X < 0)
            //{
            //    effect |= SpriteEffects.FlipHorizontally;
            //}

            //if(scale.Y < 0)
            //{
            //    effect |= SpriteEffects.FlipVertically;
            //}

            Rectangle destRect = new Rectangle((int)(pos.X), (int)(pos.Y), (int)(frame.Rect.Width * scale.X), (int)(frame.Rect.Height * scale.Y));
            spriteBatch.Draw(SpriteSheet.Texture, pos, frame.Rect, SpriteColour, rotation, origin, scale, effect, 0);
            //spriteBatch.Draw(SpriteSheet.Texture, destRect, frame.Rect, SpriteColour, rotation, origin, effect, 0);
        }
    }
}
