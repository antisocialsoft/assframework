﻿using Microsoft.Xna.Framework.Graphics;
using AssGameFramework.SceneGraph;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameObjects
{
    public class AnimationComponent : UpdatableComponent
    {
        private class AnimationDetails
        {
            public bool _playing;
            public int _currentFrame;
            public int _startFrame;
            public int _lastFrame;
            public float _frameTime;
            public float _currentFrameTime;
            public bool _loop;
            public bool _removeOnFinish;
            public int _priority;
            public int _lookup;
            public bool _finished;
        }

        private int _lookup = 0;

        //private List<Tuple<SpriteComponent, AnimationDetails>> _animationInformation = new List<Tuple<SpriteComponent, AnimationDetails>>();

        private Dictionary<SpriteComponent, List<AnimationDetails>> _animationList = new Dictionary<SpriteComponent, List<AnimationDetails>>();
        private Dictionary<int, AnimationDetails> _playingList = new Dictionary<int, AnimationDetails>();

        public AnimationComponent()
        {

        }

        public int PlayAnimation(SpriteComponent sprite, String startFrame, int numberOfFrames, float frameTime, bool loop, bool removeOnFinish, int priority)
        {
            int frame = sprite.SpriteSheet.GetIndexForFrameName(startFrame);
            Debug.Assert(frame >= 0, "Invalid frame");
            return PlayAnimation(sprite, frame, numberOfFrames, frameTime, loop, removeOnFinish, priority);
        }

        public int PlayAnimation(SpriteComponent sprite, int startFrame, int numberOfFrames, float frameTime, bool loop, bool removeOnFinish, int priority)
        {
            AnimationDetails details = new AnimationDetails();
            details._playing = true;
            details._currentFrame = startFrame;
            details._startFrame = startFrame;
            details._lastFrame = startFrame + numberOfFrames - 1;
            details._frameTime = frameTime;
            details._currentFrameTime = frameTime;
            details._loop = loop;
            details._removeOnFinish = removeOnFinish;
            details._lookup = _lookup++;
            details._finished = false;

            sprite.SetSpriteFrame(startFrame);

            if (!_animationList.ContainsKey(sprite))
            {
                _animationList[sprite] = new List<AnimationDetails>();
            }

            if (_animationList[sprite].Count == 0)
            {
                _animationList[sprite].Add(details);
            }
            else
            {
                bool inserted = false;
                for (int i = _animationList[sprite].Count - 1; i >= 0; --i)
                {
                    if (priority >= _animationList[sprite][i]._priority)
                    {
                        inserted = true;
                        _animationList[sprite].Insert(i + 1, details);
                    }
                }

                if(!inserted)
                {
                    _animationList[sprite].Insert(0, details);
                }
            }

            _playingList[details._lookup] = details;

            return details._lookup;
        }

        public bool IsPlaying(int animationIndex)
        {
            return _playingList.ContainsKey(animationIndex) && _playingList[animationIndex]._playing;
        }

        public void Resume(int animationIndex)
        {
            if(_playingList.ContainsKey(animationIndex))
            {
                AnimationDetails details = _playingList[animationIndex];
                details._playing = true;
                details._finished = false;
            }
        }

        public void Stop(int animationIndex)
        {
            if (_playingList.ContainsKey(animationIndex))
            {
                AnimationDetails details = _playingList[animationIndex];
                details._playing = false;
            }
        }

        public void Remove(int animationIndex)
        {
            if (_playingList.ContainsKey(animationIndex))
            {
                AnimationDetails details = _playingList[animationIndex];
                details._playing = false;
                details._finished = true;
                details._removeOnFinish = true;
            }
        }

        public override void FixedUpdate(float step)
        {

        }

        public override void LateFixedUpdate(float step)
        {
        }

        public override void LateUpdate(float dt)
        {
        }

        public override void StandardUpdate(float dt)
        {
            foreach (SpriteComponent spriteComp in _animationList.Keys)
            {
                List<AnimationDetails> toRemove = new List<AnimationDetails>();
                foreach (AnimationDetails details in _animationList[spriteComp])
                {
                    if (!details._playing)
                    {
                        if (details._finished && details._removeOnFinish)
                        {
                            toRemove.Add(details);
                        }
                        continue;
                    }

                    details._currentFrameTime -= dt;
                    while (details._currentFrameTime < 0)
                    {
                        details._currentFrame++;

                        if (details._currentFrame > details._lastFrame)
                        {
                            if (!details._loop)
                            {
                                details._currentFrame -= 1;
                                details._playing = false;
                                details._finished = true;

                                if (details._removeOnFinish)
                                {
                                    toRemove.Add(details);
                                }

                                break;
                            }
                            details._currentFrame = details._startFrame;
                        }
                        details._currentFrameTime += details._frameTime;
                    }
                    spriteComp.SetSpriteFrame(details._currentFrame);
                }

                foreach (AnimationDetails details in toRemove)
                {
                    _animationList[spriteComp].Remove(details);
                    _playingList.Remove(details._lookup);
                }
            }
        }
    }
}
