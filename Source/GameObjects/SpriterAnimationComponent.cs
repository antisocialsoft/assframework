﻿using AssGameFramework.SceneGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpriterDotNet;
using Microsoft.Xna.Framework.Audio;
using SpriterDotNet.Providers;
using AssGameFramework.Core;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;
using AssGameFramework.ThirdPartySupport;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace AssGameFramework.GameObjects
{
    public class SpriterAnimationComponent : UpdatableComponent
    {
        protected List<Entity> _animationEntities = new List<Entity>();
        public SpriterAnimator SpriterAnimator = null;

        protected struct SpriterLayer
        {
            public SpriterAnimator layerAnimator;
            public bool removeOnFinish;
        }

        protected List<SpriterLayer> _spriterLayers = new List<SpriterLayer>();
        protected SpriterProviderFactory _factory = null;
        protected Entity _animationEnt;

        public SpriterAnimationComponent(string animationPath, string defaultAnimation = null)
        {
            Config Config = new Config
            {
                MetadataEnabled = true,
                EventsEnabled = true,
                PoolingEnabled = true,
                TagsEnabled = true,
                VarsEnabled = true,
                SoundsEnabled = false
            };

            _factory = new SpriterProviderFactory(Config, true);

            ContentManager content = ApplicationContext.Instance.Content;

            Spriter spriter = content.Load<Spriter>(animationPath);
            string fileDir = Path.GetDirectoryName(animationPath);
            _factory.AddSetSpriterPath(spriter, fileDir);

            //foreach (SpriterFolder folder in spriter.Folders)
            //{
            //    foreach (SpriterFile file in folder.Files)
            //    {
            //        string spriterFile = fileDir + '/' + Path.GetDirectoryName(file.Name) + '/' + Path.GetFileNameWithoutExtension(file.Name);
            //        Texture2D spriteTexture = content.Load<Texture2D>(spriterFile);
            //        SpriteComponent sprite = new SpriteComponent(spriteTexture);

            //        Entity newEnt = new Entity();
            //        newEnt.Name = Path.GetFileNameWithoutExtension(file.Name) + "_Ent";
            //        _animationEntities.Add(newEnt);

            //        newEnt.AddComponent(sprite);

            //        _factory.SetSprite(spriter, folder, file, sprite);
            //    }
            //}

            SpriterAnimator = new SpriterAnimator(spriter.Entities[0], _factory);

            _animationEnt = new Entity();

            SpriterAnimator.ParentEntity = _animationEnt;

            foreach (SpriterLayer layer in _spriterLayers)
            {
                layer.layerAnimator.ParentEntity = _animationEnt;
            }

            if (defaultAnimation != null)
            {
                SpriterAnimator.Play(defaultAnimation);
                //SpriterAnimator.Speed = 0.0f;
                LateFixedUpdate(1.0f/30.0f);
                //SpriterAnimator.Speed = 1.0f;
            }
            else
            {
                SpriterAnimator.Play(spriter.Entities[0].Animations[0]);
                //SpriterAnimator.Speed = 0.0f;
                LateFixedUpdate(1.0f / 30.0f);
                //SpriterAnimator.Speed = 1.0f;
            }
        }

        public int AddAnimationLayer(String animationName, bool deleteOnFinish = true)
        {
            SpriterAnimator animator = new SpriterAnimator(SpriterAnimator.Entity, _factory);

            animator.Play(animationName);

            SpriterLayer sl = new SpriterLayer();

            sl.layerAnimator = animator;
            sl.removeOnFinish = deleteOnFinish;

            _spriterLayers.Add(sl);

            if(_animationEnt != null)
            {
                animator.ParentEntity = _animationEnt;
            }

            return _spriterLayers.Count - 1;
        }

        public void RemoveAnimationLayer(int layer)
        {
            _spriterLayers.RemoveAt(layer);
        }

        public SpriterAnimator GetAnimatorForLayer(int layer)
        {
            if(layer < _spriterLayers.Count)
            {
                return _spriterLayers[layer].layerAnimator;
            }
            else
            {
                return null;
            }
        }

        public Entity GetAnimationEntityWithName(string name)
        {
            foreach (Entity ent in _animationEntities)
            {
                if (ent.Name == name)
                    return ent;
            }

            //We may not have loaded this asset, or it may not be loading, let's look for it
            Spriter spriter = SpriterAnimator.Entity.Spriter;

            //We're looking for an asset, which will be <name>.something, so let's look for name.

            string fileName = name + ".";

            foreach (SpriterFolder folder in spriter.Folders)
                foreach (SpriterFile file in folder.Files)
                {
                    if (file.Name.StartsWith(fileName))
                    {
                        SpriteComponent sprite = _factory.GetSpriteProvider(SpriterAnimator.Entity).Get(folder.Id, file.Id);
                        if(sprite != null)
                        {
                            return sprite.Entity;
                        }
                    }
                }

            return null;
        }

        public override void LateFixedUpdate(float step)
        {
            //Remove each sprite, and re-add them if they animate
            foreach (Entity ent in _animationEntities)
            {
                //_animationEnt.RemoveChildEntity(ent);
                //foreach(SpriteComponent sprite in ent.GetComponents<SpriteComponent>())
                //{
                //    Color spriteCol = sprite.SpriteColour;
                //    spriteCol.A = 0;
                //    sprite.SpriteColour = spriteCol;
                //}
                ent.Enabled = false;
            }

            //Debug.WriteLine("Updating this frame: ");

            SpriterAnimator.Update(step * 1000.0f);

            //Debug.WriteLine("End of frame!");

            for (int i = 0; i < _spriterLayers.Count;)
            {
                SpriterLayer layer = _spriterLayers[i];
                layer.layerAnimator.Update(step * 1000.0f);

                if(layer.layerAnimator.Progress == 1.0f)
                {
                    _spriterLayers.RemoveAt(i);
                }
                else
                {
                    ++i;
                }
            }

            //Add all the entities that have just animated to remove them next frame
            _animationEntities.Clear();
            foreach (Entity ent in _animationEnt.Children)
            {
                _animationEntities.Add(ent);
            }

        }

        protected override void OnAddToEntity(Entity newParent)
        {
            newParent.AddChildEntity(_animationEnt);

            base.OnAddToEntity(newParent);
        }

        protected override void OnRemoveFromEntity()
        {
            _animationEnt.Parent.RemoveChildEntity(_animationEnt);

            base.OnRemoveFromEntity();
        }
    }
}
