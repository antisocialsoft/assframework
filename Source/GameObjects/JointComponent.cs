﻿using AssGameFramework.SceneGraph;
using FarseerPhysics.Dynamics.Joints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameObjects
{
    public class JointComponent : EntityComponent
    {
        public Joint PhysicsJoint { get; set; } = null;

        public JointComponent(Joint joint)
        {
            PhysicsJoint = joint;
        }

        public JointComponent(JointComponent otherJoint)
        {
            PhysicsJoint = otherJoint.PhysicsJoint;
        }
    }
}
