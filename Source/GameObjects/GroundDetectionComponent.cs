﻿using AssGameFramework.GameSystems;
using AssGameFramework.SceneGraph;
using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.GameObjects
{
    public class GroundDetectionComponent : EntityComponent, IUpdatableComponent
    {
        public bool OnGround
        {
        get
            {
                return _touches.Count > 0;
            }
        }

        public bool RecentlyOnGround
        {
            get
            {
                return _touches.Count > 0 || _touchlessTimer > 0;
            }
            set
            {
                if(!value)
                {
                    _touchlessTimer = -1;
                }
            }
        }

        public float GroundTimeout { get; set; }

        protected List<Contact> _touches = new List<Contact>();
        private float _touchlessTimer = 0.0f;
        private bool _testContacts = false;

        public delegate void LandedDelegate ();
        public delegate void GroundLeftDelegate ();

        public event LandedDelegate LandedEvent;
        public event GroundLeftDelegate GroundLeftEvent;

        public GroundDetectionComponent(PhysicsComponent physComp, Category groundCategory, Category selfCategory, float groundTimeout = 0.0f)
        {
            GroundTimeout = groundTimeout;

            AABB aabb;

            aabb.LowerBound = new Vector2(float.MaxValue, float.MaxValue);
            aabb.UpperBound = new Vector2(-float.MaxValue, -float.MaxValue);
            List<Fixture> fixtures = physComp.PhysicsBody.FixtureList;
            foreach (Fixture fixture in fixtures)
            {
                for (int i = 0; i < fixture.ProxyCount; ++i)
                {
                    AABB outaabb;
                    fixture.GetAABB(out outaabb, i);
                    aabb.Combine(ref aabb, ref outaabb);
                }
            }

            Fixture fix = FixtureFactory.AttachRectangle(aabb.Width * 0.8f,
                                                        10.0f * PhysicsSystem.SPRITE_TO_WORLD_SCALE,
                                                        0.01f,
                                                        aabb.Center + new Vector2(0, (aabb.Height - (10.0f * PhysicsSystem.SPRITE_TO_WORLD_SCALE)) * 0.5f),
                                                        physComp.PhysicsBody);
            fix.IsSensor = false;
            fix.OnCollision += OnGroundCollision;
            fix.OnSeparation += OnGroundSeparation;
            fix.UserData = physComp.PhysicsBody.UserData;

            fix.CollidesWith = groundCategory;
            fix.CollisionCategories = selfCategory;
        }

        protected virtual void OnGroundSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            _testContacts = true;
        }

        protected virtual bool OnGroundCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            if(contact.IsTouching && !_touches.Contains(contact))
            {
                int currentTouches = _touches.Count;
                Vector2 normal = contact.Manifold.LocalNormal;
                if (fixtureA == contact.FixtureB)
                {
                    Matrix rotation = Matrix.CreateFromAxisAngle(Vector3.UnitZ, fixtureB.Body.Rotation);
                    normal = Vector2.Transform(normal, rotation);
                    if (normal.Y == -1.0f)
                    {
                        _touches.Add(contact);
                        if (currentTouches == 0) LandedEvent.Invoke();
                        return true;
                    }
                }
                else
                {
                    Matrix rotation = Matrix.CreateFromAxisAngle(Vector3.UnitZ, fixtureB.Body.Rotation);
                    normal = Vector2.Transform(normal, rotation);
                    if (normal.Y == 1.0f)
                    {
                        _touches.Add(contact);
                        if (currentTouches == 0) LandedEvent.Invoke();
                        return true;
                    }
                }
            }
            return false;
        }

        public virtual void StandardUpdate(float dt)
        {
        }

        public virtual void FixedUpdate(float step)
        {
            if (_testContacts)
            {
                for(int i = 0; i < _touches.Count; ++i)
                {
                    if(!_touches[i].IsTouching)
                    {
                        _touches[i] = _touches[_touches.Count-1];
                        _touches.RemoveAt(_touches.Count - 1);
                        --i;
                        if (_touches.Count == 0) GroundLeftEvent.Invoke();
                    }
                }

                _testContacts = false;
            }

            if (_touchlessTimer > 0)
            {
                _touchlessTimer -= step;
            }
        }

        public virtual void LateUpdate(float dt)
        {
        }

        public virtual  void LateFixedUpdate(float step)
        {
        }
    }
}
