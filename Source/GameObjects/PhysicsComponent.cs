﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using AssGameFramework.GameSystems;
using AssGameFramework.SceneGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using AssGameFramework.Core;
using FarseerPhysics.Collision;

namespace AssGameFramework.GameObjects
{
    public class PhysicsComponent : UpdatableComponent
    {
        public Body PhysicsBody { get; private set; }

        private bool _updating = false;

        public PhysicsComponent(Body body)
        {
            PhysicsBody = body;
        }

        ~PhysicsComponent()
        {

        }

        protected override void OnDestroy()
        {
            PhysicsBody.Dispose();
        }

        public override void LateUpdate(float dt)
        {
            if (PhysicsBody != null)
            {
                _updating = true;
                if (Entity.Parent != null)
                {
                    Vector2 dif = PhysicsBody.Position * PhysicsSystem.WORLD_TO_SPRITE_SCALE - Entity.Parent.WorldPosition;
                    Entity.LocalPosition = dif;
                    float rotDif = PhysicsBody.Rotation - Entity.Parent.WorldRotation;
                    Entity.LocalRotation = rotDif;
                }
                else
                {
                    Entity.LocalPosition = PhysicsBody.Position * PhysicsSystem.WORLD_TO_SPRITE_SCALE;
                    Entity.LocalRotation = PhysicsBody.Rotation;
                }
                _updating = false;
            }
        }

        protected void OnEntityTransformChanged()
        {
            if(!_updating && PhysicsBody != null)
            {
                PhysicsBody.Position = Entity.WorldPosition * PhysicsSystem.SPRITE_TO_WORLD_SCALE;
                PhysicsBody.Rotation = Entity.WorldRotation;
            }
        }

        protected override void OnEnabled()
        {
            OnAddToEntity(Entity);
        }

        protected override void OnDisabled()
        {
            OnRemoveFromEntity();
        }

        public void SetPhysicsUserData()
        {
            //Find all the bodies and fixtures and add ourselves as the user data for collisions
            Body body = PhysicsBody;
            body.UserData = Entity;
            foreach (Fixture fix in body.FixtureList)
            {
                fix.UserData = Entity;
            }

        }

        protected override void OnAddToEntity(Entity newParent)
        {
            newParent.SubscribeToTransformChangedEvent(new Entity.TransformChangedDelegate(OnEntityTransformChanged));
            if (PhysicsBody != null)
            {
                PhysicsBody.Enabled = true;
            }

            SetPhysicsUserData();

            OnEntityTransformChanged();
        }

        protected override void OnRemoveFromEntity()
        {
            if (PhysicsBody != null)
            {
                PhysicsBody.Enabled = false;
            }
            base.OnRemoveFromEntity();
        }

        public override void OnRemovedFromScene()
        {
            OnRemoveFromEntity();
            base.OnRemoveFromEntity();
        }

        public override void OnAddedToScene()
        {
            if(PhysicsBody != null)
            {
                PhysicsBody.Enabled = true;
            }
            base.OnAddedToScene();
        }
    }
}
