﻿using AssGameFramework.StateSystem;
using AssGameFramework.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Core
{
    public interface ApplicationContextGameInterface
    {
        void SetApplicationContext(ApplicationContext context);
        void AddApplicationSystem(ISystem system);
        void SetStateMachine(StateMachine stateMachine);
        List<ISystem> ApplicationSystems { get; }

        void SetGraphicsDeviceManager(GraphicsDeviceManager graphicsManager);
        void SetContentManager(ContentManager contentManager);
        void setTextInputEvent(EventHandler<TextInputEventArgs> textInput);
    }
}
