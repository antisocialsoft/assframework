﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using AssGameFramework.StateSystem;
using System.Collections.Generic;
using AssGameFramework.Systems;
using AssGameFramework.GameSystems;
using AssGameFramework.Core;

namespace AssGameFramework.Core
{
    public abstract class AssGame : Game
    {
        protected GraphicsDeviceManager _graphics;
        protected SpriteBatch _spriteBatch;

        private float _runningFixedStepCount = 0.0f;

        protected ApplicationContext _context = new ApplicationContext();
        private ApplicationContextGameInterface _gameContext;

        public AssGame()
        {
            _graphics = new GraphicsDeviceManager(this)
            {
                PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8
            };

            Content.RootDirectory = "Content";
            _gameContext = _context;
            _gameContext.SetApplicationContext(_context);
            _gameContext.SetGraphicsDeviceManager(_graphics);
            _gameContext.SetContentManager(Content);

            _graphics.PreferredBackBufferHeight = 480;
            _graphics.PreferredBackBufferWidth = 600;

            _graphics.ApplyChanges();

            Window.TextInput += new System.EventHandler<TextInputEventArgs>(WindowTextInput);
        }

        protected void WindowTextInput(object obj, TextInputEventArgs evnt)
        {
            if(_context.TextInputEvent != null)
                _context.TextInputEvent.Invoke(obj, evnt);
        }

        protected void CreateApplicationSystems()
        {
            InputSystem inputSystem = new InputSystem();

            _gameContext.AddApplicationSystem(inputSystem);
            _gameContext.AddApplicationSystem(new AudioSystem());
        }



        abstract protected State CreateInitialState();

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        /// 
        protected override void Initialize()
        {
            StateMachine stateMachine = new StateMachine();
            _gameContext.SetStateMachine(stateMachine);
            CreateApplicationSystems();

            State intialState = CreateInitialState();
            stateMachine.PushState(intialState);
            stateMachine.UpdateStateMachine();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            StateMachine stateMachine = _context.StateMachine;
            State activeState = stateMachine.ActiveState;

            float fixedTimestep = 1.0f / 60.0f;

            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _runningFixedStepCount += dt;

            activeState.StandardUpdate(dt);

            int fixedSteps = (int)(_runningFixedStepCount / fixedTimestep);
            _runningFixedStepCount -= fixedSteps * fixedTimestep;

            for (int i = 0; i < fixedSteps; ++i)
            {
                activeState.FixedUpdate(fixedTimestep);
            }

            foreach (ISystem system in _context.ApplicationSystems)
            {
                system.StandardUpdate(dt);
                for (int i = 0; i < fixedSteps; ++i)
                {
                    system.FixedUpdate(fixedTimestep);
                }
            }

            activeState.LateUpdate(dt);

            for (int i = 0; i < fixedSteps; ++i)
            {
                activeState.LateFixedUpdate(fixedTimestep);
            }

            stateMachine.UpdateStateMachine();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            State activeState = _context.StateMachine.ActiveState;
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _context.GraphicsManager.GraphicsDevice.Clear(Color.Black);
            _context.GraphicsManager.GraphicsDevice.Clear(ClearOptions.Stencil, Color.Transparent, 0, 0);

            activeState.Draw(dt, _spriteBatch);

            base.Draw(gameTime);
        }
    }
}
