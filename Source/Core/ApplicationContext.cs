﻿using AssGameFramework.StateSystem;
using AssGameFramework.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Core
{
    public class ApplicationContext : ApplicationContextGameInterface, ApplicationContextCommonInterface
    {
        static public ApplicationContext _instance;

        static public ApplicationContextCommonInterface Instance 
            { get { return _instance; } }

        public ContentManager Content { get; private set; }
        public StateMachine StateMachine { get; private set; }

        private List<ISystem> _applicationSystems = new List<ISystem>();

        public List<ISystem> ApplicationSystems { get { return _applicationSystems; } }

        public GraphicsDeviceManager GraphicsManager { get; private set; }

        public EventHandler<TextInputEventArgs> TextInputEvent { get; set; }

        public void AddApplicationSystem(ISystem system)
        {
            _applicationSystems.Add(system);
        }

        public T GetApplicationSystem<T>() where T : ISystem
        {
            foreach (ISystem sys in _applicationSystems)
            {
                if (sys is T)
                {
                    return (T)sys;
                }
            }

            return null;
        }

        public void SetApplicationContext(ApplicationContext context)
        {
            _instance = context;
        }

        public void SetStateMachine(StateMachine stateMachine)
        {
            StateMachine = stateMachine;
        }

        public void SetContentManager(ContentManager content)
        {
            Content = content;
        }

        public void SetGraphicsDeviceManager(GraphicsDeviceManager graphicsManager)
        {
            GraphicsManager = graphicsManager;
        }

        public void setTextInputEvent(EventHandler<TextInputEventArgs> textInput)
        {
            TextInputEvent = textInput;
        }
    }
}
