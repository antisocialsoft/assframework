﻿using AssGameFramework.StateSystem;
using AssGameFramework.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Core
{
    public interface ApplicationContextCommonInterface
    {
        T GetApplicationSystem<T>() where T : ISystem;

        ContentManager Content { get; }
        StateMachine StateMachine { get; }

        GraphicsDeviceManager GraphicsManager { get; }

        EventHandler<TextInputEventArgs> TextInputEvent { get; set;  }
    };
}
