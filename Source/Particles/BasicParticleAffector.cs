﻿using AssGameFramework.GameObjects;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Particles
{
    public class BasicParticleAffectorDescription : ParticleAffectorDescription
    {
        public float MinLifeTime { get; set; } = 10.0f;
        public float LifeTimeRange { get; set; } = 0.0f;
        public float MinSpeed { get; set; } = 1.0f;
        public float SpeedRange { get; set; } = 0.0f;
        public float FinalSpeed { get; set; } = 0.0f;
        public float FinalSpeedRange { get; set; } = 0.0f;
        public float MinAngle { get; set; } = 0.0f; //Radians
        public float AngleRange { get; set; } = (float)(Math.PI * 2.0f); //Radians

        public Color StartColour { get; set; } = Color.White;
        public Color EndColour { get; set; } = Color.White;
        public float StartScale { get; set; } = 1.0f;
        public float StartScaleRange { get; set; } = 1.0f;
        public float EndScale { get; set; } = 1.0f;
        public float EndScaleRange { get; set; } = 1.0f;

        public float StartRotation { get; set; } = 0;
        public float StartRotationRange { get; set; } = 0;
        public float MinRotationSpeed { get; set; } = 0;
        public float RotationSpeedRange { get; set; } = 0;

        internal override ParticleAffector Create()
        {
            return new BasicParticleAffector(this);
        }
    }

    public class BasicParticleAffector : ParticleAffector
    {
        protected BasicParticleAffectorDescription _desc;

        protected float _targetLifeTime;
        protected float _currentLifeTime;
        protected float _speed;
        protected float _EndSpeed;

        protected Vector3 _headingDir;

        protected Color _StartColour;
        protected Color _EndColour;
        protected float _StartScale;
        protected float _EndScale;

        protected float _startRotation;
        protected float _endRotation;

        public BasicParticleAffector(BasicParticleAffectorDescription desc)
        {
            _desc = desc;
        }

        public override void Reset()
        {
            _currentLifeTime = 0;

            Random rand = new Random();

            _targetLifeTime = _desc.MinLifeTime + (float)(_desc.LifeTimeRange * rand.NextDouble());
            _speed = _desc.MinSpeed + (float)(_desc.SpeedRange * rand.NextDouble());
            _EndSpeed = _desc.MinSpeed + (float)(_desc.FinalSpeedRange * rand.NextDouble());

            Matrix mat = Matrix.CreateRotationZ(_desc.MinAngle + (float)(_desc.AngleRange * rand.NextDouble()));

            _headingDir = Vector3.Transform(Vector3.UnitY, mat);

            _StartColour = _desc.StartColour;
            _EndColour = _desc.EndColour;
            _StartScale = _desc.StartScale + (float)(_desc.StartScaleRange * rand.NextDouble());
            _EndScale = _desc.EndScale + (float)(_desc.EndScaleRange * rand.NextDouble());

            _startRotation = _desc.StartRotation + (float)(_desc.StartRotationRange * rand.NextDouble());
            _endRotation = _startRotation + ((_desc.MinRotationSpeed + (float)(_desc.RotationSpeedRange * rand.NextDouble())) * _targetLifeTime);

            base.Reset();

            FixedUpdate(0.0f);
        }

        public override void FixedUpdate(float step)
        {
            if(Finished)
            {
                return;
            }

            _currentLifeTime += step;

            float progress = Math.Min(_currentLifeTime / _targetLifeTime, 1.0f);

            //Update speed
            float currentSpeed = _speed + ((_EndSpeed - _speed) * progress);

            //update colour
            Color currentColour = Color.Lerp(_StartColour, _EndColour, progress);

            //Update scale
            float currentScale = _StartScale + ((_EndScale - _StartScale) * progress);

            //Update rotation
            float currentRotation = _startRotation + ((_endRotation - _startRotation) * progress);

            //Move
            Entity.LocalPosition3D += _headingDir * currentSpeed;
            //Change colour
            //TODO: Make colour linked to Entity
            SpriteComponent spriteComp = Entity.GetComponent<SpriteComponent>();
            if(spriteComp != null)
            {
                spriteComp.SpriteColour = currentColour;
            }
            //Change scale
            Entity.LocalScale = new Vector2(currentScale);

            //Change rotation
            Entity.LocalRotation = currentRotation;

            if(progress >= 1.0f)
            {
                Finished = true;
                Entity.SceneOwner.RemoveEntity(Entity);
            }
            
        }


    }
}
