﻿using AssGameFramework.SceneGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Particles
{
    public abstract class ParticleAffectorDescription
    {
        internal abstract ParticleAffector Create();
    }

    public class ParticleAffector : UpdatableComponent
    {
        public bool Finished { get; protected set; } = true;

        public virtual void Reset()
        {
            Finished = false;
        }

    }
}
