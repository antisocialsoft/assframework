﻿using AssGameFramework.Core;
using AssGameFramework.GameObjects;
using AssGameFramework.SceneGraph;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Particles
{

    public abstract class ParticleDescription
    {
        internal abstract Entity Create();
    }
    
    public class Texture2DParticleDescription : ParticleDescription
    {
        public string ParticleTextureName { get; set; }
        public ParticleAffectorDescription AffectorDescription { get; set; }

        internal override Entity Create()
        {
            Entity particle = new Entity();

            Texture2D particleTex = ApplicationContext.Instance.Content.Load<Texture2D>(ParticleTextureName);

            SpriteComponent spriteComp = new SpriteComponent(particleTex);
            //spriteComp.BlendState = BlendState.Additive;
            spriteComp.BlendState = BlendState.NonPremultiplied;

            particle.AddComponent(spriteComp);

            particle.AddComponent(AffectorDescription.Create());

            return particle;
        }
    }

    public class ParticlePool
    {
        public ParticleDescription ParticleDesc {get; private set;}
        public int MaxParticles { get; set; }
        public int TargetPoolSize { get; set; }
        public object AffectorDescription { get; private set; }

        private int _successCounts = 0;

        protected int LastIndex = 0;

        protected List<Entity> _particles = new List<Entity>();

        public ParticlePool(ParticleDescription desc, int maxParticles, int targetPoolSize)
        {
            ParticleDesc = desc;
            MaxParticles = maxParticles;
            TargetPoolSize = targetPoolSize;

            FillPool();
        }

        protected void FillPool()
        {
            for(int i = 0; i < TargetPoolSize; ++i)
            {
                Entity newParticle = ParticleDesc.Create();

                _particles.Add(newParticle);
            }
        }

        public Entity GetNext()
        {
            if(_successCounts > 5 && TargetPoolSize < _particles.Count)
            {
                //We've been successful enough, let's reduce the pool size
                _particles.RemoveRange(TargetPoolSize, _particles.Count - TargetPoolSize);
            }

            Predicate<Entity> search = delegate (Entity compare)
            {
                ParticleAffector affector = compare.GetComponent<ParticleAffector>();
                return affector != null && affector.Finished;
            };

            if (LastIndex >= _particles.Count) LastIndex = 0;

            for (int i = LastIndex; i < _particles.Count; ++i)
            {
                Entity availableParticle = _particles.Find(search);
                if(availableParticle != null)
                {
                    _successCounts++;
                    LastIndex = i + 1;
                    return availableParticle;
                }
            }

            //If we get this far, the rest of the pool was unavailable.
            //Check the front of the list.
            for(int i = 0; i < LastIndex; ++i)
            {
                Entity availableParticle = _particles.Find(search);
                if (availableParticle != null)
                {
                    _successCounts++;
                    LastIndex = i + 1;
                    return availableParticle;
                }
            }

            //If we get here, then there's no available particles.
            //Create new ones.
            if(_particles.Count < MaxParticles)
            {
                Entity newParticle = ParticleDesc.Create();

                _particles.Add(newParticle);

                LastIndex = 0;

                _successCounts = 0;

                return newParticle;
            }
            else
            {
                //No more particles are available.
                _successCounts = 0;
                return null;
            }

        }
    }
}
