﻿using AssGameFramework.SceneGraph;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.Particles
{
    public class ParticleEmitter : UpdatableComponent
    {
        public float MinEmitTime { get; set; } = 1.0f / 120.0f;
        public float EmitTimeRange { get; set; } = 1.0f / 30.0f;
        public float EmitLayer { get; set; } = -5.0f;

        protected float _nextEmit = 0;

        protected ParticlePool _pool;

        public ParticleEmitter(ParticlePool pool)
        {
            _pool = pool;
        }

        public override void FixedUpdate(float step)
        {
            if(!Enabled)
            {
                //Easy out
                return;
            }

            _nextEmit -= step;

            if(_nextEmit < 0)
            {
                Entity particle = _pool.GetNext();

                if(particle != null)
                {
                    particle.LocalPosition3D = Entity.LocalPosition3D + new Vector3(0, 0, EmitLayer);
                    Entity.SceneOwner.AddEntity(particle);

                    ParticleAffector affector = particle.GetComponent<ParticleAffector>();
                    affector.Reset();
                }

                _nextEmit = MinEmitTime + (float)((new Random()).NextDouble() * EmitTimeRange);
            }
        }

    }
}
