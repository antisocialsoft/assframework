﻿using AssGameFramework.GameObjects;
using AssGameFramework.SceneGraph;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using SpriterDotNet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.ThirdPartySupport
{
    public class SpriterAnimator : Animator<SpriteComponent, SoundEffect>
    {
        public Entity ParentEntity { get; set; } = null;
        public float LayerIncrement { get; set; } = 0.01f;

        private HashSet<int> _ignoreList = new HashSet<int>();
        private bool _ignoreAll = false;

        protected int _layer = 0;
        
        public SpriterAnimator(SpriterEntity entity, IProviderFactory<SpriteComponent, SoundEffect> providerFactory = null) : base(entity, providerFactory)
        {
        }

        public override void Update(float deltaTime)
        {
            _layer = 0;
            base.Update(deltaTime);
        }

        public int GetEntityIDFromName(String name)
        {
            foreach(SpriterFolder folder in this.Entity.Spriter.Folders)
                foreach(SpriterFile file in folder.Files)
            {
                    String fileName = Path.GetFileNameWithoutExtension(file.Name);
                if (fileName == name)
                {
                    return file.Id;
                }
            }

            return -1;
        }

        public void IgnoreEnt(int entID)
        {
            if (_ignoreAll)
            {
                _ignoreList.Remove(entID);
            }
            else
            {
                _ignoreList.Add(entID);
            }
        }

        public void UnignoreEnt(int entID)
        {
            if (!_ignoreAll)
            {
                _ignoreList.Remove(entID);
            }
            else
            {
                _ignoreList.Add(entID);
            }
        }

        public void IgnoreAll()
        {
            _ignoreAll = true;
            _ignoreList.Clear();
        }

        public void UnignoreAll()
        {
            _ignoreAll = false;
            _ignoreList.Clear();
        }

        protected override void ApplySpriteTransform(SpriteComponent sprite, SpriterObject info)
        {
            if(!_ignoreAll && _ignoreList.Contains(info.FileId))
            {
                _layer++;
                return;
            }
            else if(_ignoreAll && !_ignoreList.Contains(info.FileId))
            {
                _layer++;
                return;
            }

            sprite.SpritePivot = new Vector2(info.PivotX, 1.0f - info.PivotY);
            sprite.Entity.LocalRotation = MathHelper.ToRadians(-info.Angle);

            float px = info.X;
            float py = -info.Y;
            float rotation = MathHelper.ToRadians(-info.Angle);

            float rotationSin = (float)Math.Sin(0);
            float rotationCos = (float)Math.Cos(0);

            float posX = px * rotationCos - py * rotationSin + 0;
            float posY = px * rotationSin + py * rotationCos + 0;

            sprite.Entity.LocalPosition3D = new Vector3(posX, posY, LayerIncrement*_layer++);

            sprite.SpriteEffect = SpriteEffects.None;

            //if(info.ScaleX < 0)
            //{
            //    sprite.SpriteEffect |= SpriteEffects.FlipHorizontally;
            //}
            
            //if(info.ScaleY < 0)
            //{
            //    sprite.SpriteEffect |= SpriteEffects.FlipVertically;
            //}

            sprite.Entity.LocalScale = new Vector2(info.ScaleX,  info.ScaleY);

            if(sprite.Entity.Parent == null)
                ParentEntity.AddChildEntity(sprite.Entity);

            sprite.Entity.Enabled = true;

            //Debug.WriteLine(sprite.Entity.Name);

            //Color spriteCol = sprite.SpriteColour;
            //spriteCol.A = 255;
            //sprite.SpriteColour = spriteCol;
        }

        protected override void PlaySound(SoundEffect sound, SpriterSound info)
        {

        }

        protected override void DispatchEvent(string eventName)
        {
            base.DispatchEvent(eventName);
        }
    };
}
