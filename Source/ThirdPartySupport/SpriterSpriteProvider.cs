﻿using AssGameFramework.Core;
using AssGameFramework.GameObjects;
using AssGameFramework.SceneGraph;
using Microsoft.Xna.Framework.Graphics;
using SpriterDotNet;
using SpriterDotNet.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.ThirdPartySupport
{
    public class SpriterSpriteProvider : DefaultAssetProvider<SpriteComponent>
    {
        protected Spriter _spriter;
        protected string _assetLocation;

        public SpriterSpriteProvider(Spriter spriter, string animationLocation) : base()
        {
            _spriter = spriter;
            _assetLocation = animationLocation;
        }

        public SpriterSpriteProvider(Spriter spriter, string animationLocation, Dictionary<int, Dictionary<int, SpriteComponent>> assetMappings) : base(assetMappings)
        {
            _spriter = spriter;
            _assetLocation = animationLocation;
        }

        public override SpriteComponent Get(int folderId, int fileId)
        {
            //Because applying a character map relies on an existing sprite and we lazy load, fake the character map
            foreach (SpriterCharacterMap map in CharMaps)
            {
                foreach (SpriterMapInstruction mapInst in map.Maps)
                {
                    if (mapInst.FolderId == folderId && mapInst.FileId == fileId)
                    {
                        folderId = mapInst.TargetFolderId;
                        fileId = mapInst.TargetFileId;
                        break;
                    }
                }
            }

            SpriteComponent spriteComp = base.Get(folderId, fileId);

            if (spriteComp == null)
            {
                foreach (SpriterFolder folder in _spriter.Folders)
                {

                    if (folder.Id == folderId)
                    {
                        foreach (SpriterFile file in folder.Files)
                        {
                            if(file.Id == fileId)
                            {
                                string spriterFile = _assetLocation + '/' + Path.GetDirectoryName(file.Name) + '/' + Path.GetFileNameWithoutExtension(file.Name);
                                Texture2D spriteTexture = ApplicationContext.Instance.Content.Load<Texture2D>(spriterFile);
                                SpriteComponent sprite = new SpriteComponent(spriteTexture);

                                Entity newEnt = new Entity();
                                newEnt.Name = Path.GetFileNameWithoutExtension(file.Name);

                                newEnt.AddComponent(sprite);

                                spriteComp = sprite;
                                if(!AssetMappings.ContainsKey(folder.Id))
                                {
                                    AssetMappings[folderId] = new Dictionary<int, SpriteComponent>();
                                }

                                AssetMappings[folder.Id][file.Id] = spriteComp;
                                
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            return spriteComp;
        }
    }
}
