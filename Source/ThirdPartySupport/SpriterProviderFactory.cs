﻿using AssGameFramework.GameObjects;
using Microsoft.Xna.Framework.Audio;
using SpriterDotNet;
using SpriterDotNet.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssGameFramework.ThirdPartySupport
{
    public class SpriterProviderFactory : DefaultProviderFactory<SpriteComponent, SoundEffect>
    {
        protected Dictionary<Spriter, string> _fileMap = new Dictionary<Spriter, string>();


        public SpriterProviderFactory(Config config) : base(config)
        {
        }

        public SpriterProviderFactory(Config config, bool cacheAnimations) : base(config, cacheAnimations)
        {
        }

        public SpriterProviderFactory(Config config, bool cacheAnimations, int interval) : base(config, cacheAnimations, interval)
        {
        }

        public void AddSetSpriterPath(Spriter spriter, string path)
        {
            _fileMap[spriter] = path;
        }

        public override IAssetProvider<SpriteComponent> GetSpriteProvider(SpriterEntity entity)
        {
            if (!SpriteProviders.ContainsKey(entity.Spriter))
            {
                SpriteProviders[entity.Spriter] = new SpriterSpriteProvider(entity.Spriter, _fileMap[entity.Spriter]);
            }

            return SpriteProviders[entity.Spriter];
        }
    }
}
